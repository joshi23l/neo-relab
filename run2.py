"""run.py
"""
import os

import psycopg2

from utils.relab_data import refresh_relab, parse_raw_data
from utils.asteroids import list_asteroid_data, read_asteroid
from utils.analyze import compare_spectra
from utils.write import data_to_df, write_to_database
from utils.plotting import plot_ranks
from config import RELAB_DATA_DIRECTORY
from multiprocessing import Pool
from tqdm import tqdm
import click
# from sqlalchemy import create_engine
from config import NEO_RELAB_DB, client


match_only = None
do_remove_slope = False
cs_vals = {}
table_predicate = ''

list_of_asteroids = []


@click.command()
@click.option('--n-threads', '-n', default=2, help='Number of threads to use.')
@click.option('--only-source', '-s', default=None, help='Match only to the supplied relab source.')
@click.option('--get-relab', '-r', is_flag=True, help='Request that RELAB data be re-downloaded')
@click.option('--use-cache', '-c', is_flag=True, help='Use already cached neo-relab data')
def run(n_threads, only_source, get_relab, use_cache):

    global list_of_asteroids
    list_of_asteroids = list_asteroid_data()

    os.makedirs('./results/done_data/', exist_ok=True)

    n_asteroids = len(list_of_asteroids)
    assert n_asteroids > 0, 'There are no asteroid files to analyze'

    # Check that the meteorite file exists
    if not len(os.listdir(f'{RELAB_DATA_DIRECTORY}/data')) or get_relab:
        print('Meteorite data does not exist or refresh requested. Meteorite data downloading...')
        refresh_relab()
    
    # load the relab data into memory
    if not use_cache:
        parse_raw_data()

    print('Will generate plots...')
    os.makedirs('./results/plots/best/', exist_ok=True)

    global match_only
    global table_predicate
    match_only = only_source

    if match_only:
        print('Matching only [Other_Met]...')
        table_predicate += '_other_met'  # need to make this more general

    print(f'Will save to database table [results{table_predicate}]')

    n_threads = n_asteroids if n_asteroids <= n_threads else n_threads
    chunks = int(n_asteroids / n_threads)

    # start the analysis with '--thread' number of threads
    with Pool(n_threads) as p:
        print(f"Running with [{chunks}] asteroids per chunk for each of [{n_threads}] processes...")
        pbar = tqdm(p.imap_unordered(analyze_asteroid, list_of_asteroids), total=n_asteroids)
        for f in pbar:
            pbar.set_description(f"Processing {f.split('/')[-1]}")


def analyze_asteroid(asteroid):

    asteroid_data = read_asteroid(asteroid, cs_mode=cs_vals)
    # print(f'Working on asteroid [{asteroid_data["asteroid_name"]}]...')

    ranks = compare_spectra(asteroid_filename=asteroid,
                            asteroid_spectrum=asteroid_data['spectrum'],
                            albedo_range=asteroid_data['albedo_range'],
                            match_only=match_only,
                            remove_slope=do_remove_slope,
                            cs_vals=cs_vals,
                            keep_top_n=50)

    data_to_df([[asteroid_data, ranks]]).to_excel('excel_dump.xlsx')

    print('Writing to database...')
    # write_to_database(data_to_df([[asteroid_data, ranks]]), NEO_RELAB_DB, table_predicate)

    print('Plotting...')
    plot_ranks(asteroid_data, ranks)

    os.rename(asteroid, f'./results/done_data/{asteroid.split("/")[-1]}')
    return asteroid


if __name__ == '__main__':

    # run the program
    run()