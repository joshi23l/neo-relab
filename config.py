import redis

RELAB_ZIP_URL='http://www.planetary.brown.edu/relabdata/RelabDatabase2019Dec31.zip'
RELAB_DATA_DIRECTORY='./relab/data/'
RELAB_CATALOGS_DIRECTORY='./relab/data/catalogues/'

ASTEORID_DATA_DIRECTORY='./smass/filesToRun/'

# NEO_RELAB_DB = 'postgresql://postgres:postgres@localhost:5432/neorelab'
NEO_RELAB_DB = 'postgresql://'


SMASS_EXCEL = './smass/WORKING_4.0_805.xlsx'
ALBEDO_RANGES = './smass/AlbedoRanges.xlsx'


client = redis.Redis(host='localhost', port=6379)
