import psycopg2
import pandas
import math
import textwrap
# import plotly.express as px
import matplotlib.pyplot as plt

import seaborn as sns

NEO_RELAB_DB = 'postgres://bkgbyrlkylzzba:5d515de5ad982d92b7c9ffdf025974822617c1b7cab14110c1a7c162e5f9b98b@ec2-52-71-153-228.compute-1.amazonaws.com:5432/d8i9lq934pv0va'
sns.set(style='white')
sns.set_context('paper')

type_def = {'Carbonaceous Chondrite': 'CC', 'Achondrite': 'AC', 'Ordinary Chondrite': 'OC', 'Iron': 'FE',
            'Enstatite Chondrite': 'EC', 'Stony Iron': 'SFE', 'Chondrite': 'C', 'Enstatite Achondrite': 'EA',
            'Impact Melt': 'IM', 'Igneous': 'I', 'Primitive Achondrite': 'PA', 'Mixture': 'M'}

# get consistent colors across all figures
all_colors = sns.color_palette()
all_colors.extend(['k', 'gold'])
type_colors = dict(zip(type_def.keys(), all_colors))


def run():
    # grab the data
    connection = psycopg2.connect(NEO_RELAB_DB)
    # data = pandas.read_sql("SELECT * FROM neo_relab.meteorites_per_asteroid;", connection)
    data = pandas.read_sql("""SELECT COALESCE(r.type, 'No Type') AS ast_type,
                                   r.type1                     AS met_type,
                                   r.chisq,
                                   r.subtype
                            FROM neo_relab.results r
                            WHERE r.type1 IS NOT NULL
                              AND r.type IS NOT NULL
                              AND r.type NOT ILIKE '%:%'
                              AND chisq < 3
                              AND snr_2micron > 30
                              AND r.source = 'Other-Met'
                              AND r.generaltype1 = 'Rock';""", connection)
    # data['type_short'] = data.met_type.apply(lambda x: type_def.get(x, x))
    connection.close()

    # make_strip_plot(data)
    make_hist_plot(data)
    # make_hist_subclass_plot(data)


def make_hist_plot(data: pandas.DataFrame = None):
    unique_types = data.ast_type.unique()

    # n_columns = 3
    # n_types = len(unique_types)
    # fig, axes = plt.subplots(ncols=n_columns, nrows=math.ceil(n_types/n_columns), figsize=(10, 7))

    for i, t in enumerate(unique_types):
        type_data = data[data.ast_type == t]
        asteroid_type = type_data.ast_type.unique()[0]

        fig = plt.figure(figsize=(10, 7))
        g = sns.histplot(data=type_data, x="chisq", hue='met_type', palette=type_colors,
                     linewidth=1.5, element='step', binwidth=0.05, fill=False, kde=True)

        # visual modifications
        plt.setp(g.get_legend().get_texts(), fontsize='14')  # for legend text
        g.get_legend().set_title(None)
        g.get_legend().get_frame().set_alpha(0)
        plt.ylabel('Count', size=15)
        plt.xlabel('$\chi^2$', size=15)
        plt.title(f'{asteroid_type}-type', size=17)
        fig.tight_layout()

        # plt.savefig(f"/Users/brian/Desktop/hist_plots/hist_kde_plot_{asteroid_type}.png", dpi=300)
        plt.show()
        break
        plt.close(fig)

    return


def make_strip_plot(data: pandas.DataFrame = None):

    unique_types = data.ast_type.unique()

    # n_columns = 3
    # n_types = len(unique_types)
    # fig, axes = plt.subplots(ncols=n_columns, nrows=math.ceil(n_types/n_columns), figsize=(10, 7))

    for i, t in enumerate(unique_types):

        type_data = data[data.ast_type == t]
        asteroid_type = type_data.ast_type.unique()[0]

        met_counts = type_data.groupby(['met_type']).count()

        fig = plt.figure(figsize=(10, 7))

        p = sns.stripplot(x='met_type', y='chisq', hue='subtype', data=type_data, size=3, dodge=True)
        # p = sns.stripplot(x='met_type', y='chisq', data=type_data, size=3, order=met_counts.index, palette=type_colors)
        # sns.boxplot(x='met_type', y='chisq', data=type_data, order=met_counts.index, color='white')

        # wrap the xlabels
        labels = [textwrap.fill(label.get_text(), 12) for label in p.get_xticklabels()]
        p.set_xticklabels(labels)

        # annotate with the counts for each type
        for m, val in enumerate(met_counts.ast_type):
            p.annotate(f'({val})', xy=(m, 3.2), ha='center')

        plt.ylim(-0.1, 3.4)
        plt.xlabel('')
        plt.ylabel('$\chi^2$', size=15)
        plt.title(f'{asteroid_type}-type', size=17)

        fig.tight_layout()

        # plt.savefig(f"/Users/brian/Desktop/strip_plots/strip_plot_{asteroid_type}.png", dpi=300)
        plt.show()
        break
        plt.close(fig)


def make_title(a):
    asteroid_type = a.text.split("=")[-1]
    return a.update(text=f'Type={asteroid_type} (n=)')

# def make_plot(data: pandas.DataFrame = None):
    # data = data.dropna()
    # asteroid_type = data.ast_type.unique()[0]
    # # title = f'{data.type.unique()[0]}-types (n={data.n_ast_ast_by_type.unique()[0]})'
    # fig = px.bar(data, x='type_short', y='ast_match', color='type1', facet_col='type', facet_col_wrap=4,
    #              labels={'type_short': 'Meteorite Type', 'ast_match': '% Matched'}, text='n_matched_samples',
    #              color_discrete_sequence=px.colors.qualitative.Vivid, height=2000, width=1500)
    # fig.update_layout(bargap=0.1, showlegend=False, plot_bgcolor='white', paper_bgcolor='white', yaxis_range=[0, 149])
    # fig.update_traces(textposition='outside')
    # fig.update_xaxes(showline=True, linewidth=2, linecolor='black')
    # fig.update_yaxes(showline=True, linewidth=2, linecolor='black')
    # fig.for_each_annotation(lambda a: make_title(a))
    # fig.write_image("/Users/brian/Desktop/fig1.png")
    # fig.show()

    # g = sns.FacetGrid(data, col='type', col_wrap=1)
    # sns.barplot('type_short', 'ast_match', data=data).set_title('Type=S')
    # plt.savefig("/Users/brian/Desktop/fig1.png")
    # plt.show()



    # fig = plt.figure()
    # data = data[data.chisq <= 3]
    # g = sns.FacetGrid(data, row="met_type", hue="met_type", aspect=15, height=.5)
    # g.map(sns.kdeplot, "chisq", bw_adjust=.1, clip_on=False, shade=True, alpha=0.5, linewidth=0.5)
    # g.map(plt.axhline, y=0, lw=1, clip_on=False)
    #
    # def label(x, color, label):
    #     ax = plt.gca()
    #     ax.text(0, .5, determine_short_met_type(label), fontweight="bold", color=color, ha="left", va="center", transform=ax.transAxes)
    #
    # g.map(label, "chisq")
    #
    # # g.fig.subplots_adjust(hspace=0.2)
    #
    # g.set_titles("")
    # g.set(ylim=(0, 1), xlim=(-0.25, 3), yticks=[])
    # g.despine(bottom=True, left=True)

    # g = sns.FacetGrid(data, row="met_type", hue="met_type", aspect=15, height=.5)
    # g.map(sns.histplot, "chisq", element='step', clip_on=False, alpha=0.5, linewidth=0.5)
    # g.map(plt.axhline, y=0, lw=1, clip_on=False)
    #
    # def label(x, color, label):
    #     ax = plt.gca()
    #     ax.text(0, .5, determine_short_met_type(label), fontweight="bold", color=color, ha="left", va="center",
    #             transform=ax.transAxes)
    #
    # g.map(label, "chisq")
    #
    # # g.fig.subplots_adjust(hspace=0.2)
    #
    # g.set_titles("")
    # g.set(ylim=(0, None), xlim=(-0.25, 3), yticks=[])
    # g.despine(bottom=True, left=True)

    # data = data[data.chisq <= 3]
    # g = sns.FacetGrid(data, row="met_type", hue="met_type", aspect=15, height=.5)
    # g.map(sns.histplot, "chisq", clip_on=False, alpha=1, linewidth=1.5)
    # # g.map(sns.ecdfplot, "chisq", clip_on=False, color="w", lw=1)
    # g.map(plt.axhline, y=0, lw=1, clip_on=False)
    #
    # def label(x, color, label):
    #     ax = plt.gca()
    #     ax.text(0, .5, determine_short_met_type(label), fontweight="bold", color=color, ha="left", va="center", transform=ax.transAxes)
    #
    # g.map(label, "chisq")
    #
    # g.fig.subplots_adjust(hspace=0.2)

    # g.set_titles("")
    # g.set(ylim=(0, 100), xlim=(-0.25, 3), yticks=[])
    # g.despine(bottom=True, left=True)

    # g.fig.suptitle(f'{asteroid_type}-Type')

    # sns.barplot(x='ast_type', y='chisq', data=data, hue='met_type')
    # plt.savefig(f"/Users/brian/Desktop/ridge_plot_{asteroid_type}.png")
    # fig.clf()
    #
    # plt.show()

    # data.plot(x='type_short', y='ast_match', kind='bar')
    # plt.show()


if __name__ == "__main__":
    run()
