import os

import psycopg2.extras
import matplotlib.pyplot as plt
from matplotlib.patches import Patch

NEO_RELAB_DB = 'postgres://bkgbyrlkylzzba:5d515de5ad982d92b7c9ffdf025974822617c1b7cab14110c1a7c162e5f9b98b@ec2-52-71-153-228.compute-1.amazonaws.com:5432/d8i9lq934pv0va'


def run():

    all_data = gather_data()

    print('Finding unique asteroidfilenames...')
    unique_filenames = list(set([d.asteroidfilename for d in all_data]))

    print('Making plots...')
    for uf in unique_filenames:
        print(f'Working on [{uf}]...')
        make_plots([d for d in all_data if d.asteroidfilename == uf])


def gather_data():
    print("Gathering data...")
    connection = psycopg2.connect(NEO_RELAB_DB)
    cur = connection.cursor(cursor_factory=psycopg2.extras.NamedTupleCursor)
    cur.execute("""SELECT *
                    FROM (
                             SELECT *, ROW_NUMBER() OVER (PARTITION BY asteroidfilename ORDER BY chisq) _row_number
                             FROM neo_relab.temp_final_data_samples) a
                    WHERE a._row_number <= 30;""")
    data = cur.fetchall()
    connection.close()

    print("...data gathered.")
    return data


def make_plots(data):
    # there are 30 spectra
    fig = plt.figure(figsize=(18, 9))

    asteroid_filename = data[0].asteroidfilename

    major_ax = fig.add_subplot(111, frameon=False)
    major_ax.set_ylabel('Relative Reflectance\n\n\n')
    major_ax.set_xlabel('\n\nWavelength (\u03BCm)')
    major_ax.tick_params(labelleft=False, labelbottom=False, left=False, bottom=False)

    patch_colors = {f'{asteroid_filename}': 'tab:gray',
                    'Meteorite Poor Match': 'black',
                    'Meteorite Good Match': 'tab:green'}
    legend_labels = [Patch(facecolor=color, label=label) for label, color in patch_colors.items()]

    subtypes_legend = plt.legend(handles=legend_labels, loc='upper center', ncol=3, labelspacing=0,
                                 labelcolor=patch_colors.values(), bbox_to_anchor=(0.5, 1.04),
                                 borderpad=0, borderaxespad=0, frameon=False, fontsize=10)
    plt.gca().add_artist(subtypes_legend)

    # asteroid data is the same for all rows
    asteroid_data = data[0].spectrum_data['asteroid']
    asteroid_wavelength = asteroid_data['wavelength_final']
    asteroid_reflectance = asteroid_data['reflectance_final']

    # now go through each spectra/row
    for r, row in enumerate(data, start=1):
        spectrum_data = row.spectrum_data

        meteorite_wavelength = spectrum_data['meteorite']['wavelength_final']
        meteorite_reflectance = spectrum_data['meteorite']['reflectance_final']

        met_color = ('black', 'tab:green')[row.goodfit]

        ax = fig.add_subplot(5, 6, r)
        ax.scatter(asteroid_wavelength, asteroid_reflectance, s=0.8, color='tab:gray')
        ax.plot(meteorite_wavelength, meteorite_reflectance, lw=1, color=met_color)

        ax.annotate(f'{row.spectrumid}\n{row.sampleid}\n{row.type1}',
                    xy=(2.5, min(asteroid_reflectance) - 0.05), va='bottom', ha='right', fontsize=8, color=met_color)

        # formatting
        ax.set_xlim([0, 2.5])
        ax.set_ylim([min(asteroid_reflectance) - 0.1, max(asteroid_reflectance) + 0.1])
        ax.spines['top'].set_visible(False)
        ax.spines['right'].set_visible(False)

        first_col = (False, True)[(r - 1) % 6 == 0]
        ax.tick_params(labelleft=first_col, labelbottom=(False, True)[r > 24], left=first_col)
        if not first_col:
            ax.spines['left'].set_visible(False)

    plt.subplots_adjust(left=0.05, bottom=0.095, right=0.975, top=0.905)
    plt.suptitle(f'Asteroid ({asteroid_filename}) With Meteorite Matches\n')

    plt.savefig(f'christmas_plots/{asteroid_filename}_christmas_plot.png', dpi=300)
    plt.close(fig)
    return


if __name__ == "__main__":

    os.makedirs('./christmas_plots', exist_ok=True)

    run()
