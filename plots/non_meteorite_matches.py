import psycopg2
import pandas
import matplotlib.pyplot as plt
from scipy import interpolate

import numpy

import seaborn as sns

sns.set_context('paper')


ASTEROID_DATA_DIR = '/Users/brian/Work/mit/neo-relab/smass/filesToRun/'
RELAB_DATA_DIR = '/Users/brian/Work/mit/neo-relab/relab/data/data/'
NEO_RELAB_DB = 'postgres://bkgbyrlkylzzba:5d515de5ad982d92b7c9ffdf025974822617c1b7cab14110c1a7c162e5f9b98b@ec2-52-71-153-228.compute-1.amazonaws.com:5432/d8i9lq934pv0va'

TEMP_TABLES = """
WITH temp_rank AS (
    SELECT asteroidfilename,
           spectrumid,
           RANK() OVER (PARTITION BY asteroidfilename ORDER BY chisq) AS rank
    FROM neo_relab.results
    )
"""

QUERIES = {
    'Antarctic': """SELECT *
                    FROM neo_relab.results m
                             JOIN temp_rank r ON r.spectrumid = m.spectrumid AND r.asteroidfilename = m.asteroidfilename
                    WHERE type1 = 'Antarctic'
                      AND snr_2micron > 30
                      AND rank < 15
                      AND asteroid IN ('2011TN9', '5720', '2010TD54', '2017BM123', '23183', '361071', '154453')
                      AND m.spectrumid IN ('BKR1JB653', 'C1JB88', 'C1JB198', '1095F113', 'BKR1JBB57', 'C1JB197', '993F105')
                      AND m.chisq <> 0.06917427685014216
                      AND m.chisq <> 0.0822628858691141
                    ORDER BY chisq;""",
    'Earth-Igneos': """SELECT *
                        FROM neo_relab.results m
                                 JOIN temp_rank r ON r.spectrumid = m.spectrumid AND r.asteroidfilename = m.asteroidfilename
                        WHERE source = 'Earth'
                          AND generaltype1 = 'Rock'
                          AND type1 = 'Igneous'
                          AND snr_2micron > 30
                          AND rank < 15
                          AND asteroid IN ('17188', '2006UN216', '2011LJ19', '276741', '132', '887', '13553', '23183', '433', '279744')
                          AND m.spectrumid IN
                              ('C3JJ45', 'C1SG113', 'BKR1SG116A', 'C1SG109', 'C2JJ26', 'BKR1DV015IP', 'C1DV21IP', 'BKR1BU046', 'C1SP41')
                        ORDER BY chisq;""",
    'Moon-Highland': """SELECT *
                        FROM neo_relab.results m
                                 JOIN temp_rank r ON r.spectrumid = m.spectrumid AND r.asteroidfilename = m.asteroidfilename
                        WHERE source = 'Moon-Ret'
                          AND type1 = 'Highland'
                          AND snr_2micron > 30
                          AND rank < 15
                          AND asteroid IN ('538', '371660', '316720', '443892', '2014RC', '2018BP', '1916', '719')
                          AND m.spectrumid IN ('BKR1LR108', 'CALS81', 'CLS083', 'C1LR107', 'C3LU07', 'CLS290', 'CGLS68', 'C1LS128')
                          AND m.chisq <> 0.11380693535602507
                          AND m.chisq <> 1.4851881786376757 --cherry picking out duplicate matches
                        ORDER BY chisq;""",
    'Moon-Mare': """SELECT *
                    FROM neo_relab.results m
                             JOIN temp_rank r ON r.spectrumid = m.spectrumid AND r.asteroidfilename = m.asteroidfilename
                    WHERE source = 'Moon-Ret'
                      AND (type1 = 'Mare' OR type1 = 'Mare Breccia')
                      AND snr_2micron > 30
                      AND rank < 15
                      AND asteroid IN ('2018WD2', '2017AE5', '3552', '297274', '2012QE50', '2017YE5', '1627')
                      AND m.spectrumid IN ('C1LS121', 'C1LS122', 'C1LR58', 'SILS49', 'NBLS48', 'CLS147', 'CHLS49')
                      AND m.chisq <> 0.2119864646779998
                      AND m.chisq <> 1.4633610363461362
                    ORDER BY chisq;"""
}
QUERIES = {key: TEMP_TABLES + query for key, query in QUERIES.items()}


def run():

    (ct1, ct1_l), (ct2, ct2_l), (st, st_l), (mm, mm_l) = (
        do_queries(QUERIES['Antarctic']),
        do_queries(QUERIES['Earth-Igneos']),
        do_queries(QUERIES['Moon-Highland']),
        do_queries(QUERIES['Moon-Mare']),
    )

    fig, (ax1, ax2, ax3, ax4) = plt.subplots(4, 1, figsize=(13, 8))
    xlim = [0.3, 22]
    ylim = [0.8, 2.0]
    y_val = 1.8
    ax1.scatter(ct1.wavelength, ct1.asteroid_reflectance, marker='o', s=2, clip_on=False)
    ax1.scatter(ct1.wavelength, ct1.meteorite_reflectance, marker='.', s=1.3, color='black', clip_on=False)
    # plot the asteroid names and types
    asteroids_text = [f'{a} ({at})\n{m}' for a, at, m in zip(ct1_l['asteroid_name'],
                                                                        ct1_l['asteroid_type'],
                                                                        ct1_l['meteorite_spectrum'])]
    for text, x_val in zip(asteroids_text, ct1_l['min_x_val']):
        ax1.annotate(text, xy=(x_val, y_val), fontsize='x-small')

    ax1.spines['right'].set_visible(False)
    ax1.spines['top'].set_visible(False)
    ax1.spines['bottom'].set_visible(False)
    ax1.xaxis.set_ticks_position('none')
    ax1.set(xticklabels=[])
    ax1.set_xlim(xlim)
    ax1.set_ylim(ylim)
    ax1.set_title('Antarctic', fontdict={'fontsize': '14'}, loc='left')

    ax2.scatter(ct2.wavelength, ct2.asteroid_reflectance, marker='.', s=2, clip_on=False)
    ax2.scatter(ct2.wavelength, ct2.meteorite_reflectance, marker='.', s=1.3, color='black', clip_on=False)
    # plot the asteroid names and types
    asteroids_text = [f'{a} ({at})\n{m}' for a, at, m in zip(ct2_l['asteroid_name'],
                                                                        ct2_l['asteroid_type'],
                                                                        ct2_l['meteorite_spectrum'])]
    for text, x_val in zip(asteroids_text, ct2_l['min_x_val']):
        ax2.annotate(text, xy=(x_val, y_val), fontsize='x-small')

    ax2.spines['right'].set_visible(False)
    ax2.spines['bottom'].set_visible(False)
    ax2.spines['top'].set_visible(False)
    ax2.xaxis.set_ticks_position('none')
    ax2.set(xticklabels=[])
    ax2.set_xlim(xlim)
    ax2.set_ylim(ylim)
    ax2.set_title('Earth-Igneous', fontdict={'fontsize': '14'}, loc='left')

    ax3.scatter(st.wavelength, st.asteroid_reflectance, marker='.', s=2, clip_on=False)
    ax3.scatter(st.wavelength, st.meteorite_reflectance, marker='.', s=1.3, color='black', clip_on=False)
    # plot the asteroid names and types
    asteroids_text = [f'{a} ({at})\n{m}' for a, at, m in zip(st_l['asteroid_name'],
                                                                        st_l['asteroid_type'],
                                                                        st_l['meteorite_spectrum'])]
    for text, x_val in zip(asteroids_text, st_l['min_x_val']):
        ax3.annotate(text, xy=(x_val, y_val), fontsize='x-small')

    ax3.spines['right'].set_visible(False)
    ax3.spines['bottom'].set_visible(False)
    ax3.spines['top'].set_visible(False)
    ax3.xaxis.set_ticks_position('none')
    ax3.set(xticklabels=[])
    ax3.set_xlim(xlim)
    ax3.set_ylim(ylim)
    ax3.set_title('Lunar-Highland', fontdict={'fontsize': '14'}, loc='left')

    ax4.scatter(mm.wavelength, mm.asteroid_reflectance, marker='.', s=2, clip_on=False)
    ax4.scatter(mm.wavelength, mm.meteorite_reflectance, marker='.', s=1.3, color='black', clip_on=False)
    # plot the asteroid names and types
    asteroids_text = [f'{a} ({at})\n{m}' for a, at, m in zip(mm_l['asteroid_name'],
                                                                        mm_l['asteroid_type'],
                                                                        mm_l['meteorite_spectrum'])]
    for text, x_val in zip(asteroids_text, mm_l['min_x_val']):
        ax4.annotate(text, xy=(x_val, y_val), fontsize='x-small')

    ax4.spines['right'].set_visible(False)
    ax4.spines['bottom'].set_visible(False)
    ax4.spines['top'].set_visible(False)
    ax4.xaxis.set_ticks_position('none')
    ax4.set(xticklabels=[])
    ax4.set_xlim(xlim)
    ax4.set_ylim(ylim)
    ax4.set_title('Lunar-Mare', fontdict={'fontsize': '14'}, loc='left')

    plt.tight_layout()
    plt.subplots_adjust(hspace=0.4, bottom=0.05, top=0.935)
    plt.savefig('/Users/brian/Desktop/non_meteorite_plot2.png', dpi=300)
    plt.show()


def do_queries(query):
    data = gather_results(query)
    all_data = {'match': [],
                'wavelength': [],
                'asteroid_reflectance': [],
                'meteorite_reflectance': []}
    label_data = {'asteroid_name': [],
                  'asteroid_type': [],
                  'meteorite_spectrum': [],
                  'meteorite_type': [],
                  'min_x_val': []}

    spacing = list(range(0, 10))
    for i, d in enumerate(data):
        a_data = read_asteroid_file(d['asteroidfilename'])
        norm = a_data.loc[(a_data['wavelength'] - 1).abs().argsort()[:1]]
        a_norm_value = norm.reflectance.values[0]
        norm_index = norm.index[0]
        a_data['reflectance'] = a_data.reflectance / a_norm_value

        # shift spectra for plotting
        this_wavelengths = list(a_data.wavelength + (spacing[i] * 2.1))
        all_data['wavelength'].extend(this_wavelengths)

        all_data['match'].extend([f"{d['asteroidfilename']}-{d['spectrumid']}"] * len(a_data.wavelength))

        r_data = read_relab_file(d['relab_data_path'])
        f = interpolate.interp1d(r_data.wavelength, r_data.reflectance, kind='cubic')
        m_ref = list(f(a_data.wavelength))

        # doge the spectra purely for plotting
        all_data['asteroid_reflectance'].extend(list(a_data.reflectance))
        all_data['meteorite_reflectance'].extend([(m / m_ref[norm_index]) for m in m_ref])

        label_data['asteroid_name'] += [d['asteroid']]
        label_data['asteroid_type'] += [d['type']]
        label_data['meteorite_type'] += [''.join(t[0] for t in d['type1'].split())]
        label_data['meteorite_spectrum'] += [d['spectrumid']]
        label_data['min_x_val'] += [min(this_wavelengths)]

    return pandas.DataFrame(all_data), label_data


def gather_results(query):
    # grab the data
    connection = psycopg2.connect(NEO_RELAB_DB)
    # data = pandas.read_sql("SELECT * FROM neo_relab.meteorites_per_asteroid;", connection)
    data = pandas.read_sql(query, connection).to_dict('records')
    connection.close()
    return data


def read_asteroid_file(a_file):
    data = pandas.read_csv(f'{ASTEROID_DATA_DIR}{a_file}', delimiter='\s+', usecols=[0, 1],
                           names=['wavelength', 'reflectance'])
    data.sort_values(by=['wavelength'], inplace=True)  # make sure they are sorted propery
    data = data[data.reflectance > 0]
    return data


def read_relab_file(r_file):
    with open(f"{RELAB_DATA_DIR}{r_file}", 'r') as r_data:
        try:
            data = pandas.read_csv(r_data, delimiter='\t', header=1, dtype=numpy.float64, usecols=[0, 1])
            data.rename(columns={'Wavelength(micron)': 'wavelength', 'Reflectance': 'reflectance'}, inplace=True)

        except (pandas.errors.ParserError, ValueError):
            print('Something went wrong with parsing, try they harder way...')
            r_data.seek(0)
            r_data.readline()  # just need to skip the first two lines
            r_data.readline()
            data = []
            for line in r_data:
                split_line = line.rstrip().split()
                if len(split_line) >= 2:
                    try:
                        data.append([float(split_line[0]), float(split_line[1])])

                    except Exception:
                        print(f'Couldnt parse line [{line.rstrip()}] in file [{r_file}]')
                        pass

            data = pandas.DataFrame(data=data, dtype=numpy.float64).rename(columns={0: 'wavelength',
                                                                                    1: 'reflectance'})
        data.dropna(inplace=True)
        data = data[data['reflectance'] > 0]  # Drop where the reflectance is 0 (probably an error?)
        data.sort_values(by=['wavelength'], inplace=True)  # make sure they are sorted propery

        # drop duplicates -- because of interpolate.iterp1d
        data.drop_duplicates(subset=['wavelength'], keep='first', inplace=True)

        return data


if __name__ == "__main__":
    run()
