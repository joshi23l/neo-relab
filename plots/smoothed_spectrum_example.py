
import psycopg2.extras
import matplotlib.pyplot as plt
import numpy
from scipy import signal

NEO_RELAB_DB = 'postgres://bkgbyrlkylzzba:5d515de5ad982d92b7c9ffdf025974822617c1b7cab14110c1a7c162e5f9b98b@ec2-52-71-153-228.compute-1.amazonaws.com:5432/d8i9lq934pv0va'
QUERY = '''
SELECT *
FROM neo_relab.temp_final_data
WHERE asteroidfilename = 'a002912.sp04.txt'
LIMIT 1;
'''


def query():

    print('Retrieving data...')
    connection = psycopg2.connect(NEO_RELAB_DB)
    cur = connection.cursor(cursor_factory=psycopg2.extras.NamedTupleCursor)
    cur.execute(QUERY)

    data = cur.fetchall()
    cur.close()
    connection.close()

    return data


def _calc_fit(x, y, around=0.55):

    # get the closest wavelength to `normalize_wavelength`
    closest_index, closest_value = min(enumerate(x), key=lambda w: abs(w[1]-around))
    if abs(closest_value - around) > 0.5:
        return None

    closest_index = 10 if closest_index < 10 else closest_index

    # do a first order extrapolation if we are at the very beginning of the meteorite spectrum
    fit_order = 2 if closest_index else 1

    # fit a polynomial around the normalize wavelength (2nd degree)
    fit = numpy.poly1d(numpy.polyfit(x[closest_index - 10:closest_index + 10],
                                     y[closest_index - 10:closest_index + 10], fit_order))
    return fit


def _normalize(wavelength, reflectance, normalization_wavelength=None):

    if not normalization_wavelength:
        normalization_wavelength = 0.55 if min(wavelength) <= 0.55 else 1.215

    # calculate the value at `normalize_wavelength`
    normalization_value = _calc_fit(wavelength, reflectance, normalization_wavelength)(normalization_wavelength)
    normalized_reflectance = [r / normalization_value for r in reflectance]

    return normalized_reflectance, normalization_wavelength


def _filter(wavelength, reflectance):
    # sort the asteroid by wavelengths
    wavelength, reflectance = zip(*sorted(zip(wavelength, reflectance)))

    # smooth the asteroid https://scipy.github.io/devdocs/generated/scipy.signal.savgol_filter.html
    filtered_reflectance = signal.savgol_filter(reflectance, 21, 3)

    return wavelength, filtered_reflectance


def make_plot(data):

    asteroid_data = data.spectrum_data['asteroid']
    original_wavelength = asteroid_data['reflectance_original']     # they are backwards
    original_reflectance = asteroid_data['wavelength_original']

    a_reflectance_normalized, normalization_wavelength = _normalize(original_wavelength, original_reflectance)
    smoothed_wavelength, smoothed_reflectance = _filter(original_wavelength, a_reflectance_normalized)

    plt.figure(figsize=(10, 7))

    plt.plot(original_wavelength, original_reflectance, label='Original', color='black', alpha=0.8, lw=2)
    plt.plot(smoothed_wavelength, smoothed_reflectance, label='Smoothed', color='blue', lw=1)
    plt.legend(fontsize=12, frameon=False)

    plt.xticks(fontsize=12)
    plt.yticks(fontsize=12)

    plt.title('Asteroid (2912) Lapalma', fontsize=16)
    plt.ylabel('Relative Reflectance', fontsize=14)
    plt.xlabel('Wavelength (\u03BCm)', fontsize=14)

    plt.tight_layout()
    plt.savefig('asteroid_2912_smoothed_example.png', dpi=300)
    # plt.show()


if __name__ == '__main__':
    make_plot(query()[0])
