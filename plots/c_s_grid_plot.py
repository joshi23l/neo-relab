import psycopg2
import pandas
import matplotlib.pyplot as plt
from scipy import interpolate

import numpy

import seaborn as sns

sns.set_context('paper')


ASTEROID_DATA_DIR = '/Users/brian/Work/mit/neo-relab/smass/filesToRun/'
RELAB_DATA_DIR = '/Users/brian/Work/mit/neo-relab/relab/data/data/'
NEO_RELAB_DB = 'postgres://bkgbyrlkylzzba:5d515de5ad982d92b7c9ffdf025974822617c1b7cab14110c1a7c162e5f9b98b@ec2-52-71-153-228.compute-1.amazonaws.com:5432/d8i9lq934pv0va'

TEMP_TABLES = """
WITH temp_rank AS (
    SELECT asteroidfilename,
           spectrumid,
           RANK() OVER (PARTITION BY asteroidfilename ORDER BY chisq) AS rank
    FROM neo_relab.results),
     temp_nspec AS (
         SELECT "Source" as source,
                "GeneralType1" as generaltype1,
                "Type1" as type1,
                COUNT(DISTINCT "SpectrumID") as nspec,
                COUNT(DISTINCT LEFT(sp."SampleID", 10)) as nsample
         FROM neo_relab.spectra_catalog sp
                  LEFT OUTER JOIN neo_relab.sample_catalog sc ON sc."SampleID" = sp."SampleID"
         WHERE "Start" <= 900
           AND "Stop" >= 2450
         GROUP BY "Source", "GeneralType1", "Type1"
         ORDER BY COUNT(DISTINCT LEFT(sp."SampleID", 10)) DESC)
"""

C_TYPES_1_QUERY = TEMP_TABLES + """
--QUERY 1: C-types GROUP 1. C-types that may be S-types and match to OCs
SELECT m.asteroidfilename,
       m.asteroid,
       m.type,
       m.sampleid,
       m.spectrumid,
       m.type1,
       m.subtype,
       m.chisq,
       m.relab_data_path,
       m.cleaned_subtype
FROM neo_relab.results m
         JOIN temp_rank r ON r.spectrumid = m.spectrumid AND r.asteroidfilename = m.asteroidfilename
         LEFT OUTER JOIN temp_nspec n ON n.source = m.source AND n.generaltype1 = m.generaltype1 AND n.type1 = m.type1
WHERE (type ILIKE 'C%' OR type = 'X%' OR type ILIKE 'B%' OR type ILIKE 'D%' OR type ILIKE 'T%')
  AND m.source = 'Other-Met'
  AND m.generaltype1 = 'Rock'
  AND m.type1 = 'Ordinary Chondrite'
  AND snr_2micron > 30
  AND rank < 15
  AND m.asteroidfilename IN ('a007092.nir.txt', 'a099935.nir.txt', 'a162635.nir.txt', 'a283457.nir.txt',
                             'au2011SR69.nir.txt') --S-like C-types
  AND m.spectrumid IN ('CDMH51', 'C1MH24', 'C1TB89', 'CCMP28')
ORDER BY m.asteroidfilename, chisq;
"""

C_TYPES_2_QUERY = TEMP_TABLES + """
--QUERY 2: C-types Group 2: C-types that match OCs, but the OCs don't have typical spectra
SELECT m.asteroidfilename,
       m.asteroid,
       m.type,
       m.sampleid,
       m.spectrumid,
       m.type1,
       m.subtype,
       m.chisq,
       m.relab_data_path,
       m.cleaned_subtype
FROM neo_relab.results m
         JOIN temp_rank r ON r.spectrumid = m.spectrumid AND r.asteroidfilename = m.asteroidfilename
         LEFT OUTER JOIN temp_nspec n ON n.source = m.source AND n.generaltype1 = m.generaltype1 AND n.type1 = m.type1
WHERE (type ILIKE 'C%' OR type = 'X%' OR type ILIKE 'B%' OR type ILIKE 'D%' OR type ILIKE 'T%')
  AND m.source = 'Other-Met'
  AND m.generaltype1 = 'Rock'
  AND m.type1 = 'Ordinary Chondrite'
  AND snr_2micron > 30
  AND rank < 15
  AND m.asteroidfilename NOT IN
      ('a000088.sp263n1.txt', 'a000128.sp263n1.txt', 'a002099.visnir.txt', 'a002100.visnir.txt', 'a007350.nir.txt',
       'a009068.nir.txt', 'a026760.visnir.txt',
       'a108519.visnir.txt', 'a153219.nir.txt', 'a159504.sp213.txt', 'a162581.nir.txt', 'a170502.visnir.txt',
       'a175706.visnir.txt', 'a025330.visnir.txt', 'a285263.visnir.txt', 'a312473.visnir.txt', 'a419464.nir.txt',
       'a523811.sp248n1.txt', 'au2006RZ.nir.txt', 'au2017bm123.sp264n1.txt')
  AND m.asteroidfilename NOT IN ('a007092.nir.txt', 'a099935.nir.txt', 'a162635.nir.txt', 'a283457.nir.txt',
                                 'au2011SR69.nir.txt') --CC like OCs --all slabs, chips or large >300um grains, reflectance values are all < 0.1 as opposed to typical OC reflectance of
  AND m.spectrumid IN ('S1LM05', 'MGN149', 'C1MA52', 'C1MH20', 'C1MT217', 'C1MH20', 'C4MH21')
  AND m.chisq <> 0.06114490844670512
  AND m.chisq <> 0.04183752879772155
ORDER BY m.asteroidfilename, chisq;
"""

S_TYPES_QUERY = TEMP_TABLES + """
SELECT m.asteroidfilename,
       m.asteroid,
       m.type,
       m.sampleid,
       m.spectrumid,
       m.type1,
       m.subtype,
       m.chisq,
       m.relab_data_path,
       m.cleaned_subtype
FROM neo_relab.results m
         JOIN temp_rank r ON r.spectrumid = m.spectrumid AND r.asteroidfilename = m.asteroidfilename
         LEFT OUTER JOIN temp_nspec n ON n.source = m.source AND n.generaltype1 = m.generaltype1 AND n.type1 = m.type1
WHERE (type ILIKE 'S%' OR type ILIKE 'Q%')
  AND m.source = 'Other-Met'
  AND m.generaltype1 = 'Rock'
  AND m.type1 = 'Carbonaceous Chondrite'
  AND snr_2micron > 30
  AND rank < 15
  AND m.asteroidfilename NOT IN
      ('a000433.sp245n2.txt', 'a000433.sp256n2.txt', 'a002059.sp257.txt', 'a006456.visnir.txt', 'a013551.sp215.txt',
       'a085818.visnir.txt', 'a143487.nir.txt', 'a217807.visnir.txt', 'a226514.sp225.txt', 'a267729.nir.txt',
       'a303959.sp216.txt',
       'a337866.sp215.txt')
  AND m.asteroidfilename NOT IN
      ('a477885.sp267.txt', 'au2013PJ10.nir.txt', 'au2017ae5.sp263n2.txt', 'a409995.nir.txt', 'a409995.dm19n1.txt',
       'a162510.sp216.txt', 'au2005JR5.nir.txt', 'au2013ux14.sp272.txt')
   AND m.spectrumid IN ('C2LM12','C1MT47','C1MC05','C1MT17','BKR1MT315','C1TM06','CAMS40','C2MT17')
ORDER BY m.asteroidfilename, chisq;
"""


def run():

    (ct1, ct1_l), (ct2, ct2_l), (st, st_l) = (do_queries(C_TYPES_1_QUERY), do_queries(C_TYPES_2_QUERY), do_queries(S_TYPES_QUERY))

    fig, (ax1, ax2, ax3) = plt.subplots(3, 1, figsize=(13, 7))
    xlim = [0, 22]
    ylim = [0.8, 1.75]
    y_val = 1.5
    ax1.scatter(ct1.wavelength, ct1.asteroid_reflectance, marker='o', s=2)
    ax1.scatter(ct1.wavelength, ct1.meteorite_reflectance, marker='.', s=1.3, color='black')
    # plot the asteroid names and types
    asteroids_text = [f'{a}\n({at})\n\n{m}\n({mt})' for a, at, m, mt in zip(ct1_l['asteroid_name'],
                                                                        ct1_l['asteroid_type'],
                                                                        ct1_l['meteorite_spectrum'],
                                                                        ct1_l['meteorite_type'])]
    for text, x_val in zip(asteroids_text, ct1_l['min_x_val']):
        ax1.annotate(text, xy=(x_val, y_val), fontsize='x-small', va='center')

    ax1.spines['right'].set_visible(False)
    ax1.spines['top'].set_visible(False)
    ax1.spines['bottom'].set_visible(False)
    ax1.xaxis.set_ticks_position('none')
    ax1.set(xticklabels=[])
    ax1.set_xlim(xlim)
    ax1.set_ylim(ylim)
    ax1.set_title('C-/X-complex and Ordinary Chondrites (Group 1)', fontdict={'fontsize': '14'}, loc='left')

    ax2.scatter(ct2.wavelength, ct2.asteroid_reflectance, marker='.', s=2)
    ax2.scatter(ct2.wavelength, ct2.meteorite_reflectance, marker='.', s=1.3, color='black')
    # plot the asteroid names and types
    asteroids_text = [f'{a}\n({at})\n\n{m}\n({mt})' for a, at, m, mt in zip(ct2_l['asteroid_name'],
                                                                        ct2_l['asteroid_type'],
                                                                        ct2_l['meteorite_spectrum'],
                                                                        ct2_l['meteorite_type'])]
    for text, x_val in zip(asteroids_text, ct2_l['min_x_val']):
        ax2.annotate(text, xy=(x_val, y_val), fontsize='x-small', va='center')

    ax2.spines['right'].set_visible(False)
    ax2.spines['bottom'].set_visible(False)
    ax2.spines['top'].set_visible(False)
    ax2.xaxis.set_ticks_position('none')
    ax2.set(xticklabels=[])
    ax2.set_xlim(xlim)
    ax2.set_ylim(ylim)
    ax2.set_title('C-/X-complex and Ordinary Chondrites (Group 2)', fontdict={'fontsize': '14'}, loc='left')

    ax3.scatter(st.wavelength, st.asteroid_reflectance, marker='.', s=2)
    ax3.scatter(st.wavelength, st.meteorite_reflectance, marker='.', s=1.3, color='black')
    # plot the asteroid names and types
    asteroids_text = [f'{a}\n({at})\n\n{m}\n({mt})' for a, at, m, mt in zip(st_l['asteroid_name'],
                                                                        st_l['asteroid_type'],
                                                                        st_l['meteorite_spectrum'],
                                                                        st_l['meteorite_type'])]
    for text, x_val in zip(asteroids_text, st_l['min_x_val']):
        ax3.annotate(text, xy=(x_val, y_val), fontsize='x-small', va='center')

    ax3.spines['right'].set_visible(False)
    ax3.spines['bottom'].set_visible(False)
    ax3.spines['top'].set_visible(False)
    ax3.xaxis.set_ticks_position('none')
    ax3.set(xticklabels=[])
    ax3.set_xlim(xlim)
    ax3.set_ylim(ylim)
    ax3.set_title('S-complex and Carbonaceous Chondrites', fontdict={'fontsize': '14'}, loc='left')

    plt.tight_layout()
    plt.subplots_adjust(hspace=0.4)
    plt.savefig('/Users/brian/Desktop/grids_plot1.png', dpi=300)
    # plt.show()


def do_queries(query):
    data = gather_results(query)
    all_data = {'match': [],
                'wavelength': [],
                'asteroid_reflectance': [],
                'meteorite_reflectance': []}
    label_data = {'asteroid_name': [],
                  'asteroid_type': [],
                  'meteorite_spectrum': [],
                  'meteorite_type': [],
                  'min_x_val': []}

    spacing = list(range(0, 10))
    for i, d in enumerate(data):
        a_data = read_asteroid_file(d['asteroidfilename'])
        norm = a_data.loc[(a_data['wavelength'] - 1).abs().argsort()[:1]]
        a_norm_value = norm.reflectance.values[0]
        norm_index = norm.index[0]
        a_data['reflectance'] = a_data.reflectance / a_norm_value

        # shift spectra for plotting
        this_wavelengths = list(a_data.wavelength + (spacing[i] * 2.1))
        all_data['wavelength'].extend(this_wavelengths)

        all_data['match'].extend([f"{d['asteroidfilename']}-{d['spectrumid']}"] * len(a_data.wavelength))

        r_data = read_relab_file(d['relab_data_path'])
        f = interpolate.interp1d(r_data.wavelength, r_data.reflectance, kind='cubic')
        m_ref = list(f(a_data.wavelength))

        # doge the spectra purely for plotting
        all_data['asteroid_reflectance'].extend(list(a_data.reflectance))
        all_data['meteorite_reflectance'].extend([(m / m_ref[norm_index]) for m in m_ref])

        label_data['asteroid_name'] += [d['asteroid']]
        label_data['asteroid_type'] += [d['type']]
        label_data['meteorite_type'] += [d['cleaned_subtype']]
        label_data['meteorite_spectrum'] += [d['spectrumid']]
        label_data['min_x_val'] += [min(this_wavelengths)]

    return pandas.DataFrame(all_data), label_data


def gather_results(query):
    # grab the data
    connection = psycopg2.connect(NEO_RELAB_DB)
    # data = pandas.read_sql("SELECT * FROM neo_relab.meteorites_per_asteroid;", connection)
    data = pandas.read_sql(query, connection).to_dict('records')
    connection.close()
    return data


def read_asteroid_file(a_file):
    data = pandas.read_csv(f'{ASTEROID_DATA_DIR}{a_file}', delimiter='\s+', usecols=[0, 1],
                           names=['wavelength', 'reflectance'])
    data.sort_values(by=['wavelength'], inplace=True)  # make sure they are sorted propery
    data = data[data.reflectance > 0]
    return data


def read_relab_file(r_file):
    with open(f"{RELAB_DATA_DIR}{r_file}", 'r') as r_data:
        try:
            data = pandas.read_csv(r_data, delimiter='\t', header=1, dtype=numpy.float64, usecols=[0, 1])
            data.rename(columns={'Wavelength(micron)': 'wavelength', 'Reflectance': 'reflectance'}, inplace=True)

        except (pandas.errors.ParserError, ValueError):
            print('Something went wrong with parsing, try they harder way...')
            r_data.seek(0)
            r_data.readline()  # just need to skip the first two lines
            r_data.readline()
            data = []
            for line in r_data:
                split_line = line.rstrip().split()
                if len(split_line) >= 2:
                    try:
                        data.append([float(split_line[0]), float(split_line[1])])

                    except Exception:
                        print(f'Couldnt parse line [{line.rstrip()}] in file [{r_file}]')
                        pass

            data = pandas.DataFrame(data=data, dtype=numpy.float64).rename(columns={0: 'wavelength',
                                                                                    1: 'reflectance'})
        data.dropna(inplace=True)
        data = data[data['reflectance'] > 0]  # Drop where the reflectance is 0 (probably an error?)
        data.sort_values(by=['wavelength'], inplace=True)  # make sure they are sorted propery

        # drop duplicates -- because of interpolate.iterp1d
        data.drop_duplicates(subset=['wavelength'], keep='first', inplace=True)

        return data


if __name__ == "__main__":
    run()
