import pandas
import psycopg2
import numpy
import matplotlib.pyplot as plt
from matplotlib import rcParams
from scipy import interpolate, stats, signal
import os

# plt.switch_backend('Agg')
rcParams['legend.handlelength'] = 0
NEO_RELAB_DB = 'postgres://bkgbyrlkylzzba:5d515de5ad982d92b7c9ffdf025974822617c1b7cab14110c1a7c162e5f9b98b@ec2-52-71-153-228.compute-1.amazonaws.com:5432/d8i9lq934pv0va'


def run():

    print("Gathering data...")
    asteroid_types, data = gather_data()

    dict_data = [{'type': a_type, 'data': data[data.type == a_type]} for a_type in asteroid_types]
    total_len = len(dict_data)
    for i, d in enumerate(dict_data):
        make_plots(d)
        print(f'Done {i+1}/{total_len}', end='\r')
    print('\nDone')


def gather_data():

    connection = psycopg2.connect(NEO_RELAB_DB)
    # cur = connection.cursor()
    # cur.execute(f")Select * from neo_relab.fn_final_data('neo_relab.{table}', 30")
    # connection.commit()

    # data = pandas.read_sql('SELECT * FROM neo_relab.temp_final_data WHERE goodfit;', connection)
    data = pandas.read_sql("""
        SELECT *
            FROM neo_relab.temp_final_data
            WHERE goodfit
              AND subtype = 'CM'
              AND type NOT IN ('Ch', 'Cgh')
              AND particulate = 'Yes'
              AND samplename NOT ILIKE '%laser%'
              AND text NOT ILIKE '%thermal%'
            ORDER BY type;""",
                           connection)
    connection.close()

    unique_types = data.type.unique()
    return unique_types, data


def _calc_fit(x, y, around=0.55):

    # get the closest wavelength to `normalize_wavelength`
    closest_index, closest_value = min(enumerate(x), key=lambda w: abs(w[1]-around))
    if abs(closest_value - around) > 0.5:
        return None

    closest_index = 10 if closest_index < 10 else closest_index

    # do a first order extrapolation if we are at the very beginning of the meteorite spectrum
    fit_order = 2 if closest_index else 1

    # fit a polynomial around the normalize wavelength (2nd degree)
    fit = numpy.poly1d(numpy.polyfit(x[closest_index - 10:closest_index + 10],
                                     y[closest_index - 10:closest_index + 10], fit_order))
    return fit


def _normalize(wavelength, reflectance, normalization_wavelength=None):

    if not normalization_wavelength:
        normalization_wavelength = 0.55 if min(wavelength) <= 0.55 else 1.215

    # calculate the value at `normalize_wavelength`
    normalization_value = _calc_fit(wavelength, reflectance, normalization_wavelength)(normalization_wavelength)
    normalized_reflectance = [r / normalization_value for r in reflectance]

    return normalized_reflectance


def _filter(wavelength, reflectance):
    # sort the asteroid by wavelengths
    wavelength, reflectance = zip(*sorted(zip(wavelength, reflectance)))

    # smooth the asteroid https://scipy.github.io/devdocs/generated/scipy.signal.savgol_filter.html
    filtered_reflectance = signal.savgol_filter(reflectance, 21, 3)

    return wavelength, filtered_reflectance


def _interpolate(wavelength, reflectance, interpolate_to_wavels):

    # interpolation_wavelengths = numpy.linspace(0.5)

    f = interpolate.interp1d(wavelength, reflectance, kind='cubic')
    interpolated_flux = f(interpolate_to_wavels)

    return interpolated_flux


def make_plots(d):

    asteroid_type = d['type']
    data = d['data']
    unique_asteroids = data.asteroidfilename.unique()

    savedir = f"/Users/brian/Desktop/inspectionGrids/temp_final_data/{asteroid_type}/"
    os.makedirs(savedir, exist_ok=True)

    for u_asteroid in unique_asteroids:

        # fig, axes = plt.subplots(nrows=n_rows, ncols=n_cols, sharex=True, sharey=True, figsize=(18, 9))

        print(f'Working on asteroid {u_asteroid}', end='\r')
        fig = plt.figure(figsize=(18, 9))
        legend_list = []
        legend_labels = []

        plots = []

        this_asteroid = data[data.asteroidfilename == u_asteroid]
        for match_row in this_asteroid.itertuples():

            asteroid = match_row.spectrum_data['asteroid']
            meteorite = match_row.spectrum_data['meteorite']

            # chisq = sum([((o - e)**2) / e for o, e in zip(m_reflect, a_reflect)])
            chisq = stats.chisquare(meteorite['reflectance_final'], f_exp=asteroid['reflectance_final'])[0]

            plots.append({'meteorite': {'wavel': meteorite['wavelength_final'],
                                        'refl': meteorite['reflectance_final']},
                          'asteroid': {'wavel': asteroid['wavelength_final'],
                                       'refl': asteroid['reflectance_final']},
                          'chisq': chisq,
                          'match_row': match_row})

        # plots = sorted(plots, key=lambda x: x['chisq'])[:30]
        for c, p in enumerate(plots[:30], start=1):
            ax = fig.add_subplot(5, 6, c)

            a_wavel, a_reflect = (p['asteroid']['wavel'], p['asteroid']['refl'])
            m_wavel, m_reflect = (p['meteorite']['wavel'], p['meteorite']['refl'])
            chisq = p['chisq']
            match_row = p['match_row']

            ax.scatter(a_wavel, a_reflect, marker='.', s=2)
            mplt = ax.scatter(a_wavel, m_reflect, marker='.', s=1.3, color='black')

            # if c % 6:
            #     y_label = "\n".join(u_asteroid.replace(".txt", "").split('.'))
            #     ax.set_ylabel(f'{y_label}')

            ax.spines['right'].set_visible(False)
            ax.spines['top'].set_visible(False)
            # ax.spines['bottom'].set_visible(False)
            if (c - 1) % 6:
                ax.spines['left'].set_visible(False)

            ax.tick_params(labelleft=(False, True)[(c - 1) % 6 == 0], labelbottom=(False, True)[c > 24], left=False)

            leg = ax.legend(loc='lower center', title=f'{c} ({chisq})', frameon=False, labels=[], prop={'size': 5})
            for text in leg.get_texts():
                text.set_color(("black", "red")[match_row.goodfit])

            ax.set_xlim([0, 2.5])
            ax.set_ylim([0.5, 1.5])

            legend_list.append(mplt)
            legend_labels.append(f'{c}. {match_row.sampleid}: Chi-Sq={round(match_row.chisq, 4)}\n'
                                 f'Albedo={match_row.albedo_reflectance:.2f}\n'
                                 f'{match_row.source}:{match_row.generaltype1}:{match_row.type1}\n'
                                 f'{match_row.subtype}')

        fig.legend(legend_list, legend_labels, loc='center right', title=f"{u_asteroid.replace('.txt', '')} ({asteroid_type}-Type)",
                   ncol=2, prop={'size': 7}, numpoints=None, borderpad=5, labelspacing=1, frameon=False)

        fig.subplots_adjust(wspace=0.08, hspace=0.05, right=0.78, left=0.05, top=0.975, bottom=0.05)
        fig.savefig(f'{savedir}{u_asteroid}.png', dpi=300)
        plt.close(fig)   # close the figure to reclaim memory


if __name__ == "__main__":
    # mp.set_start_method('forkserver')
    run()
#
# {"asteroid": {
#     "wavelength_final": '',
#     "reflectance_final": '',
#     "wavelength_original": '',
#     "reflectance_original": ''
# },
#     "meteorite": {
#         "wavelength_final": '',
#         "reflectance_final": '',
#         "wavelength_original": '',
#         "reflectance_original": ''}
# }
