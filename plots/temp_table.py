def temp_table(table: str = 'results'):
    return f"""
--%%%%%%%%%%%%%%%%%%% START TEMP TABLE WORK %%%%%%%%%%%%%%%%%%%%%%%%%

--meteorite spectra by type available in all of RELAB 
DROP TABLE IF EXISTS temp_nspec;
CREATE TEMP TABLE temp_nspec
(
    source          text,
    generaltype1    text,
    type1           text,
    subtype         text,
    nspec           integer,
    nsample         integer,
    nspec_subtype   integer,
    nsample_subtype integer
);
INSERT INTO temp_nspec (source, generaltype1, type1, subtype, nspec_subtype, nsample_subtype)
    (SELECT "Source",
            "GeneralType1",
            "Type1",
            LOWER(CASE
                      WHEN "SubType" ILIKE '%diogenite%' THEN 'HED Diogenite'
                      WHEN "SubType" ILIKE '%eucrite%' THEN 'HED Eucrite'
                      WHEN "SubType" ILIKE '%Howardite%' THEN 'HED Howardite'
                      ELSE RTRIM(REGEXP_REPLACE("SubType", '[^a-zA-Z]', ' ', 'g')) END) AS subtype,
            COUNT(DISTINCT "SpectrumID"),
            COUNT(DISTINCT LEFT(sp."SampleID", 10)) --count(DISTINCT sp."SampleID"),
     FROM neo_relab.spectra_catalog sp
              LEFT OUTER JOIN neo_relab.sample_catalog sc ON sc."SampleID" = sp."SampleID"
     WHERE "Start" <= 900
       AND "Stop" >= 2450
     GROUP BY "Source", "GeneralType1", "Type1",
              LOWER(CASE
                        WHEN "SubType" ILIKE '%diogenite%' THEN 'HED Diogenite'
                        WHEN "SubType" ILIKE '%eucrite%' THEN 'HED Eucrite'
                        WHEN "SubType" ILIKE '%Howardite%' THEN 'HED Howardite'
                        ELSE RTRIM(REGEXP_REPLACE("SubType", '[^a-zA-Z]', ' ', 'g')) END)
     ORDER BY COUNT(DISTINCT LEFT(sp."SampleID", 10)) DESC);

UPDATE temp_nspec t
SET nspec=f.nspec,
    nsample=f.nsample
FROM (SELECT "Source"                                AS source,
             "GeneralType1"                          AS generaltype1,
             "Type1"                                 AS type1,
             COUNT(DISTINCT "SpectrumID")            AS nspec,
             COUNT(DISTINCT LEFT(sp."SampleID", 10)) AS nsample --count(DISTINCT sp."SampleID"),
      FROM neo_relab.spectra_catalog sp
               LEFT OUTER JOIN neo_relab.sample_catalog sc ON sc."SampleID" = sp."SampleID"
      WHERE "Start" <= 900
        AND "Stop" >= 2450
      GROUP BY "Source", "GeneralType1", "Type1"
      ORDER BY COUNT(DISTINCT LEFT(sp."SampleID", 10)) DESC) f
WHERE t.source = f.source
  AND t.generaltype1 = f.generaltype1
  AND t.type1 = f.type1;
--select * from temp_nspec ;

--select * from temp_nspec where source ='Other-Met'  order by type1 ,subtype
--select source,generaltype1,type1,nspec,nsample from temp_nspec where source ='Other-Met' group by source,generaltype1,type1,nspec,nsample
--select * from neo_relab.sample_catalog where "SubType" ilike '%diogenite%'
--select * from neo_relab.spectra_catalog where "SampleID" ilike 'MB-TXH-067%'
--select * from neo_relab.sample_catalog where "Type1" ='Chondrite'


--%%%%%%%%%%%%%%%%%%% END TEMP TABLE WORK %%%%%%%%%%%%%%%%%%%%%%%%%
"""
