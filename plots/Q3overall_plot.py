import psycopg2
import pandas
import textwrap
from matplotlib import cm
from matplotlib.patches import Patch
from tqdm import tqdm
import matplotlib.pyplot as plt
import seaborn as sns
import numpy
import re
import click
import os

NEO_RELAB_DB = 'postgres://bkgbyrlkylzzba:5d515de5ad982d92b7c9ffdf025974822617c1b7cab14110c1a7c162e5f9b98b@ec2-52-71-153-228.compute-1.amazonaws.com:5432/d8i9lq934pv0va'
sns.set(style='white')
sns.set_context('paper')

TYPES = ['LL', 'L', 'H', 'CI', 'CK', 'CM', 'CO', 'CR', 'CV', 'TL', 'E', 'EH', 'EL', 'R', 'Acapulcoite-Lodranite',
         'Aubrite', 'Brachinite', 'Diogenite', 'Eucrite', 'Howardite', 'Ureilite',
         'Ureilite Anomalous Polymict', 'Pallasite', 'Almahata Sitta', 'Mesosiderite']
SORT_ORDER = dict(zip(TYPES, range(len(TYPES))))

QUERY_3_OVERALL = """
--Query 3 --Subtype breakdown
--asteroid type per meteorite --60% of OCs matched to an asteroid of any class
SELECT f.source,
       f.generaltype1,
       f.type1,
       f.subtype,
       SUM(n_matchbyclass)                                                  AS tot_matches,
       g.n_matches,
       f.nspec_subtype,
       g.n_matchsamples,
       f.nsample_subtype,
       f.nspec,
       ROUND((n_matches::numeric / nspec_subtype::numeric) * 100, 1)        AS frac_metmatch,    --matches by subtype
       ROUND((n_matchsamples::numeric / nsample_subtype::numeric) * 100, 1) AS frac_samplematch, --matches by subtype for sample
       JSON_OBJECT_AGG(type, n_matchbyclass)                                AS n_matchbyclass,
       JSON_OBJECT_AGG(type, n_distinctspectra)                             AS n_distinctspectra,
       JSON_OBJECT_AGG(type, n_distinctsamples)                             AS  asteroids
FROM (SELECT COUNT(*)                             AS n_matchbyclass,
             COUNT(DISTINCT m.spectrumid)         AS n_distinctspectra,
             COUNT(DISTINCT LEFT(m.sampleid, 10)) AS n_distinctsamples,
             m.source,
             m.generaltype1,
             m.type1,
             m.subtype,
             m.type,
             n.nspec,
             n.nspec_subtype,
             n.nsample_subtype
      FROM neo_relab.temp_final_data_samples m
               LEFT OUTER JOIN neo_relab.nspec n
                               ON n.source = m.source AND n.generaltype1 = m.generaltype1 AND n.type1 = m.type1 AND
                                  n.subtype = m.subtype
      WHERE type IS NOT NULL --and m.source = 'Other-Met' and m.generaltype1='Rock'
      and goodfit
      GROUP BY m.source, m.generaltype1, m.type1, m.subtype, type, nspec, nspec_subtype, nsample_subtype
               --HAVING count(*) > 3
      ORDER BY source, m.generaltype1, m.type1, m.subtype, n_matchbyclass DESC ----asteroid types per meteorite
     ) f
         LEFT OUTER JOIN (SELECT m.source,
                                 m.generaltype1,
                                 m.type1,
                                 m.subtype,
                                 COUNT(DISTINCT m.spectrumid)         AS n_matches,
                                 COUNT(DISTINCT LEFT(m.sampleid, 10)) AS n_matchsamples
                          FROM neo_relab.temp_final_data_samples m
                          WHERE type IS NOT NULL -- and m.source = 'Other-Met' and m.generaltype1='Rock'
                          and goodfit
                          GROUP BY m.source, m.generaltype1, m.type1, m.subtype) g
                         ON g.source = f.source AND g.type1 = f.type1 AND g.generaltype1 = f.generaltype1 AND
                            g.subtype = f.subtype
WHERE f.subtype IN
      ('Acapulcoite-Lodranite','Brachinite','Almahata Sitta', 'Howardite', 'Eucrite', 'Diogenite', 'Ureilite', 'Aubrite', 'CM',
       'CV', 'CO', 'CR', 'CI', 'CK', 'R', 'EH', 'E', 'EL', 'L', 'LL', 'H', 'Pallasite', 'TL','Mesosiderite')
  AND f.type1 NOT IN ('Enstatite Achondrite')
  AND f.source = 'Other-Met'
  AND f.generaltype1 = 'Rock'
  AND f.type1 IS NOT NULL
GROUP BY f.source, f.generaltype1, f.type1, f.subtype, f.nspec, g.n_matches, f.nspec_subtype, f.nsample_subtype,
         g.n_matchsamples, f.nsample_subtype
HAVING SUM(n_matchbyclass) >= 1
ORDER BY source, type1, subtype DESC NULLS LAST;
"""


@click.command()
@click.argument('table', nargs=1, type=str)
def run(table):
    # grab the data
    print('Querying the data...')
    connection = psycopg2.connect(NEO_RELAB_DB)
    # with connection as conn:
    #     with conn.cursor() as cur:
    #         cur.execute(f"SELECT * FROM neo_relab.fn_final_data('neo_relab.{table}', 30)")

    overall_data = pandas.read_sql(QUERY_3_OVERALL, connection)
    connection.close()

    all_asteroids = []
    for a in overall_data.asteroids.tolist():
        all_asteroids.extend(list(a.keys()))
    unique_asteroids = list(set(all_asteroids))

    # define colors
    s_asteroids = sorted([s for s in unique_asteroids if re.match('^[SsQq]', s)])
    c_asteroids = sorted([c for c in unique_asteroids if re.match('^[CcBb]', c)])
    x_asteroids = sorted([x for x in unique_asteroids if re.match('^[XxDd]', x)])
    k_asteroids = sorted([k for k in unique_asteroids if re.match('^[Kk]', k)])
    l_asteroids = sorted([l for l in unique_asteroids if re.match('^[Ll]', l)])
    v_asteroids = sorted([v for v in unique_asteroids if re.match('^[Vv]', v)])
    other_asteroids = sorted([o for o in unique_asteroids if re.match('[^][SsQqCcBbXxDdKkLlVv]', o)])

    s_colors = list(cm.Reds_r(numpy.linspace(0.2, 0.8, len(s_asteroids))))
    c_colors = list(cm.Blues(numpy.linspace(0.3, 0.8, len(c_asteroids))))
    # x_colors = list(cm.Greens_r(numpy.linspace(0.1, 0.9, len(x_asteroids))))
    x_colors = list(cm.Purples(numpy.linspace(0.3, 0.7, len(x_asteroids))))
    k_colors = list(cm.Greys(numpy.linspace(0.4, 0.6, len(k_asteroids))))
    # l_colors = list(cm.Purples(numpy.linspace(0.7, 0.8, len(l_asteroids))))
    l_colors = list(cm.Oranges(numpy.linspace(0.5, 0.8, len(l_asteroids))))
    # v_colors = list(cm.YlOrBr(numpy.linspace(0.5, 0.6, len(v_asteroids))))
    v_colors = list(cm.Greens_r(numpy.linspace(0.2, 1, len(x_asteroids))))
    other_colors = list(cm.Pastel1(numpy.linspace(0.6, 0.9, len(other_asteroids))))

    legend_colors = {'S': '#bc141a', 'C': '#1764ab', 'X': "#6c54a5",
                     'K': "#b5b5b5", 'L': "#fd8c3b", 'V': "#157f3b", }
    # 'Other': other_colors}.items()}

    asteroid_colors = dict(
        zip(s_asteroids + c_asteroids + x_asteroids + k_asteroids + l_asteroids + v_asteroids + other_asteroids,
            s_colors + c_colors + x_colors + k_colors + l_colors + v_colors + other_colors))

    overall_bar_plot(overall_data, asteroid_colors, table, legend_colors)


def overall_bar_plot(data, asteroid_colors, table, legend_colors):

    print(asteroid_colors)

    subtypes = data.subtype.tolist()
    subtypes = sorted(subtypes, key=lambda x: SORT_ORDER.get(x))

    fig, ax = plt.subplots(figsize=(14, 8))
    ax.bar(subtypes, [1] * len(subtypes), alpha=0)

    labels = [textwrap.fill(label, 12) for label in subtypes + ['Iron']]
    ax.set_xticks(subtypes + ['Iron'])
    ax.set_xticklabels(labels)

    pbar = tqdm(enumerate(subtypes), total=len(subtypes))
    for x_value, met_subtype in pbar:

        this_subtype = data[data.subtype == met_subtype]
        asteroids = this_subtype.asteroids.tolist()[0]
        total_asteroids = sum(asteroids.values())
        total_height = this_subtype.frac_samplematch  # percent

        ax.annotate(f'{this_subtype.n_matchsamples.tolist()[0]}/{this_subtype.nsample_subtype.tolist()[0]}',
                    xy=(x_value, total_height + 2), ha='center', va='center', fontsize=10)

        previous_height = 0
        for asteroid, count in reversed(sorted(asteroids.items(), key=lambda x: x[1])):
            this_height = float((count / total_asteroids) * total_height)
            ax.bar(x_value, this_height, color=asteroid_colors[asteroid], bottom=previous_height)

            ax.annotate(f'{(asteroid, "")[this_height <= 2.5]}', xy=(x_value, previous_height + this_height / 2),
                        ha='center', va='center', fontsize=10)
            previous_height += this_height

        pbar.set_description(f'Processed [{met_subtype}]')

    # add in the Irons manually
    x_value += 1
    previous_height = 0
    iron_asteroids = { "D" : 8, "X" : 4, "Xk" : 1, "Xc" : 1 }
    ax.annotate(f'{11}/{19}', xy=(x_value, 56.5), ha='center', va='center', fontsize=10)
    for asteroid, count in iron_asteroids.items():
        this_height = float((count / 15) * 57.9)
        print(asteroid)
        ax.bar(x_value, this_height, color=asteroid_colors[asteroid], bottom=previous_height)
        ax.annotate(f'{(asteroid, "")[this_height <= 2.5]}', xy=(x_value, previous_height + this_height / 2),
                    ha='center', va='center', fontsize=10)
        previous_height += this_height

    ax.tick_params(axis='y', labelsize=10, left=True, direction='in')
    ax.tick_params(axis='x', bottom=True, labelsize=8, direction='inout')
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)

    ax.set_ylim([0, 80])
    ax.set_yticks(ax.get_yticks())
    ax.set_yticklabels([f'{t:.0f}%' for t in ax.get_yticks()])

    ax.set_xlim([-0.7, len(subtypes) + 1 - 0.3])
    plt.ylabel('Percent of meteorite samples that matched to an asteroid', fontdict={'fontsize': 14})

    legend_labels = [Patch(facecolor=color, label=ast_type) for ast_type, color in asteroid_colors.items()]

    subtypes_legend = plt.legend(handles=legend_labels, loc='upper center', ncol=7, labelspacing=0,
                                 labelcolor=asteroid_colors.values(),
                                 borderpad=0, borderaxespad=0, frameon=False, fontsize=16)
    plt.gca().add_artist(subtypes_legend)

    fig.tight_layout()
    savedir = f'/Users/brian/Desktop/FrancescaQueries/Q3overall/{table}/'
    os.makedirs(savedir, exist_ok=True)
    plt.savefig(f'{savedir}meteorite-by-asteroid-subtypes-overall_visnir.png', dpi=300)
    plt.show()
    plt.close(fig)


if __name__ == '__main__':
    run()
