import psycopg2
import pandas
import textwrap
from matplotlib import cm
import matplotlib.pyplot as plt
from tqdm import tqdm
import seaborn as sns
import numpy
import re
import click
import os

plt.switch_backend('Agg')
NEO_RELAB_DB = 'postgres://bkgbyrlkylzzba:5d515de5ad982d92b7c9ffdf025974822617c1b7cab14110c1a7c162e5f9b98b@ec2-52-71-153-228.compute-1.amazonaws.com:5432/d8i9lq934pv0va'
sns.set(style='white')
sns.set_context('paper')


QUERY_2 = """
--%%%%%%%%%%%%%%%%%%% QUERY 2 %%%%%%%%%%%%%%%%%%%%%%%%%
--asteroid type per meteorite --60% of OCs matched to an asteroid of any class
SELECT f.source,
       f.generaltype1,
       f.type1,
       SUM(n_matchbyclass)                                          AS tot_matches,
       g.n_matches,
       f.nspec,
       f.nsample,
       ROUND((n_matches::numeric / nspec::numeric) * 100, 1)        AS frac_metmatch,
       ROUND((n_matchsamples::numeric / nsample::numeric) * 100, 1) AS frac_samplematch, --matches by subtype for sample
       JSON_OBJECT_AGG(type, n_matchbyclass)                        AS n_matchbyclass,
       JSON_OBJECT_AGG(type, n_distinctspectra)                     AS n_distinctspectra,
       JSON_OBJECT_AGG(type, n_distinctsamples)                     AS asteroids -- n_distinctsamples
FROM (SELECT COUNT(*)                             AS n_matchbyclass,
             COUNT(DISTINCT m.spectrumid)         AS n_distinctspectra,
             COUNT(DISTINCT LEFT(m.sampleid, 10)) AS n_distinctsamples,
             m.source,
             m.generaltype1,
             m.type1,
             type,
             nspec,
             n.nsample
      FROM neo_relab.temp_final_data m
               LEFT OUTER JOIN (SELECT source,
                                       generaltype1,
                                       type1,
                                       nspec,
                                       (CASE WHEN type1 = 'Achondrite' THEN nsample - 19 ELSE nsample END) AS nsample
                                FROM neo_relab.nspec
                                GROUP BY source, generaltype1, type1, nspec, nsample) n
                               ON n.source = m.source AND n.generaltype1 = m.generaltype1 AND n.type1 = m.type1
      WHERE type IS NOT NULL --and m.source='Other-Met' and m.generaltype1='Rock'
      and goodfit
            --  and m.subtype <> 'Ureilite Anomalous Polymict' --Exclude Almahatta Sitta - individual asteroid that can be seen in subtype plot.
      GROUP BY m.source, m.generaltype1, m.type1, type, nspec, n.nsample
      ORDER BY source, m.generaltype1, m.type1, n_matchbyclass DESC ----asteroid types per meteorite
     ) f
         LEFT OUTER JOIN (SELECT m.source,
                                 m.generaltype1,
                                 m.type1,
                                 COUNT(DISTINCT m.spectrumid)         AS n_matches,
                                 COUNT(DISTINCT LEFT(m.sampleid, 10)) AS n_matchsamples
                          FROM neo_relab.temp_final_data m
                          WHERE type IS NOT NULL -- and m.source='Other-Met' and m.generaltype1='Rock'
                                -- and subtype <> 'Ureilite Anomalous Polymict'
                        and goodfit
                          GROUP BY m.source, m.generaltype1, m.type1) g
                         ON g.source = f.source AND g.type1 = f.type1 AND g.generaltype1 = f.generaltype1
WHERE f.source = 'Other-Met'
  AND f.generaltype1 = 'Rock'
  AND f.type1 NOT IN ('Impact Melt', 'Enstatite Achondrite', 'Primitive Achondrite', 'Chondrite')
  AND f.type1 IS NOT NULL
GROUP BY f.source, f.generaltype1, f.type1, f.nspec, f.nsample, g.n_matches, g.n_matchsamples
ORDER BY tot_matches DESC;
"""


@click.command()
@click.argument('table', nargs=1, type=str)
def run(table):

    # grab the data
    print('Querying the data...')
    connection = psycopg2.connect(NEO_RELAB_DB)
    # with connection as conn:
        # with conn.cursor() as cur:
        #     cur.execute(f"SELECT * FROM neo_relab.fn_final_data('neo_relab.{table}', 30)")

    data = pandas.read_sql(QUERY_2, connection)
    connection.close()

    all_asteroids = []
    for a in data.asteroids.tolist():
        all_asteroids.extend(list(a.keys()))
    unique_asteroids = list(set(all_asteroids))

    # define colors
    s_asteroids = sorted([s for s in unique_asteroids if re.match('^[SsQq]', s)])
    c_asteroids = sorted([c for c in unique_asteroids if re.match('^[CcBb]', c)])
    x_asteroids = sorted([x for x in unique_asteroids if re.match('^[XxDd]', x)])
    k_asteroids = sorted([k for k in unique_asteroids if re.match('^[Kk]', k)])
    l_asteroids = sorted([l for l in unique_asteroids if re.match('^[Ll]', l)])
    v_asteroids = sorted([v for v in unique_asteroids if re.match('^[Vv]', v)])
    other_asteroids = sorted([o for o in unique_asteroids if re.match('[^][SsQqCcBbXxDdKkLlVv]', o)])

    s_colors = list(cm.Reds_r(numpy.linspace(0.2, 0.8, len(s_asteroids))))
    c_colors = list(cm.Blues(numpy.linspace(0.3, 0.8, len(c_asteroids))))
    x_colors = list(cm.Greens_r(numpy.linspace(0.1, 0.9, len(x_asteroids))))
    k_colors = list(cm.Greys(numpy.linspace(0.3, 0.5, len(k_asteroids))))
    l_colors = list(cm.Purples(numpy.linspace(0.7, 0.8, len(l_asteroids))))
    v_colors = list(cm.YlOrBr(numpy.linspace(0.5, 0.6, len(v_asteroids))))
    other_colors = list(cm.Pastel1(numpy.linspace(0.6, 0.9, len(other_asteroids))))

    asteroid_colors = dict(
        zip(s_asteroids + c_asteroids + x_asteroids + k_asteroids + l_asteroids + v_asteroids + other_asteroids,
            s_colors + c_colors + x_colors + k_colors + l_colors + v_colors + other_colors))

    make_bar_plot(data, asteroid_colors, table)


def make_bar_plot(data: pandas.DataFrame, asteroid_colors, table):

    meteorites = data.type1.tolist()

    fig, ax = plt.subplots(figsize=(14, 8))
    ax.bar(meteorites, [1] * len(meteorites))

    labels = [textwrap.fill(label, 12) for label in meteorites]
    ax.set_xticks(meteorites)
    ax.set_xticklabels(labels)
    ax.set_yticks(ax.get_yticks())
    ax.set_yticklabels([f'{t * 100:.0f}%' for t in ax.get_yticks()])

    print('Working on plots...')
    pbar = tqdm(enumerate(meteorites))
    for m, met in pbar:
        asteroids = data[data.type1 == met].asteroids.tolist()[0]
        total_asteroids = sum(asteroids.values())

        ax.annotate(f'n={total_asteroids}', xy=(m, 1.05), ha='center', va='center', fontsize=10)

        asteroid_height = 1   # percent
        for asteroid, count in sorted(asteroids.items(), key=lambda x: x[1]):
            this_height = float(asteroid_height)
            ax.bar(m, asteroid_height, color=asteroid_colors[asteroid])
            asteroid_fraction = count/total_asteroids
            asteroid_height -= asteroid_fraction

            middle_height = asteroid_height + (this_height - asteroid_height) / 2
            ax.annotate(f'{(asteroid, "")[asteroid_fraction <= 0.025]}', xy=(m, middle_height), ha='center', va='center', fontsize=10)

        pbar.set_description(f'Processed [{met}]')

    ax.tick_params(axis='y', labelsize=10, left=True, direction='in')
    ax.tick_params(axis='x', bottom=True, labelsize=8, direction='inout')
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)

    ax.set_ylim([0, 1.1])
    ax.set_xlim([-0.7, len(meteorites) - 0.3])
    plt.ylabel('Cumulative percent of asteroid spectral\nmatches to each meteorite class', fontdict={'fontsize': 14})
    fig.tight_layout()

    savedir = f'/Users/brian/Desktop/FrancescaQueries/Q2/{table}/'
    os.makedirs(savedir, exist_ok=True)
    plt.savefig(f'{savedir}meteorite-by-asteroid_visnir.png', dpi=300)
    plt.close(fig)


if __name__ == '__main__':
    run()
