import matplotlib.pyplot as plt
import pandas


def run():
    data = pandas.read_excel('./mesosiderite.xlsx', skiprows=1)

    plt.figure(figsize=(10, 6))

    plt.plot(
        data['Mesosiderite Powder Wavelength'],
        data['Mesosiderite Powder Reflectance'],
        color='tab:blue',
        label='Mesosiderite Powder',
        linewidth=3
    )

    sorted_asteroid_wavel, sorted_asteroid_reflect = zip(
        *sorted(zip(data['Asteroid 2851 Wavelength'], data['Asteroid 2851 Reflectance']))
    )

    plt.plot(
        sorted_asteroid_wavel,
        sorted_asteroid_reflect,
        color='tab:orange',
        label='Asteroid 2851',
        linewidth=3
    )

    plt.plot(
        data['RELAB Mesosiderite Wavelength'][data['RELAB Mesosiderite Wavelength'] < 2.5],
        data['RELAB Mesosiderite Reflectance'][data['RELAB Mesosiderite Wavelength'] < 2.5],
        color='gray',
        label='RELAB Mesosiderite',
        linewidth=3
    )

    plt.title('Mesosiderite Meteorites and V-type Asteroids')
    plt.xlabel('Wavelength (\u03BCm)')
    plt.ylabel('Relative Reflectance')

    plt.legend(loc='lower center', ncol=3, frameon=False)
    plt.tight_layout()
    plt.savefig('mesosiderite_plot.png')
    plt.show()


if __name__ == "__main__":
    run()
