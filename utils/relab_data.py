from config import RELAB_ZIP_URL, RELAB_DATA_DIRECTORY

import requests
import zipfile
import io
import json
import os
import pandas
import numpy
from config import client


def refresh_relab(url=RELAB_ZIP_URL, extract_to=RELAB_DATA_DIRECTORY):

    os.makedirs(RELAB_DATA_DIRECTORY, exist_ok=True)

    print('Performing a full download of the RELAB spectra data. This will take some time...')
    r = requests.get(url)

    print('...Download complete. Extracting...')
    z = zipfile.ZipFile(io.BytesIO(r.content))
    z.extractall(path=extract_to)

    print('...Extraction complete.')


def parse_raw_data(data_directory=f'{RELAB_DATA_DIRECTORY}/data'):
    print('Parsing out the meteorite data...')
    relab_samples = []
    for root, dirs, files in os.walk(data_directory, topdown=False):
        for file_name in files:
            split_name = file_name.split('.')
            if len(split_name) != 2:   # this is not an appropriatley named file
                continue

            ext = split_name[-1]
            if ext == 'txt':
                with open(os.path.join(root, file_name), 'r') as data_file:
                    try:
                        data = pandas.read_csv(data_file, delimiter='\t', header=1, dtype=numpy.float64, usecols=[0,1])

                    except (pandas.errors.ParserError, ValueError):
                        print('Something went wrong with parsing, try they harder way...')
                        data_file.seek(0)
                        data_file.readline()    # just need to skip the first two lines
                        data_file.readline()
                        data = []
                        for line in data_file:
                            split_line = line.rstrip().split()
                            if len(split_line) >= 2:
                                try:
                                    data.append([float(split_line[0]), float(split_line[1])])

                                except Exception:
                                    print(f'Couldnt parse line [{line.rstrip()}] in file [{file_name}]')
                                    pass

                        data = pandas.DataFrame(data=data, dtype=numpy.float64).rename(columns={0: 'Wavelength(micron)',
                                                                                                1: 'Reflectance'})

                    # If the minimum wavelength is > 0.9 then we don't want to consider this meterorite
                    # If the maximum wavelength is < 2.45 then we don't want to consider this meteorite
                    if data['Wavelength(micron)'].min() > 0.9 or data['Wavelength(micron)'].max() < 2.45:
                        continue

                    data.dropna(inplace=True)
                    data = data[data['Reflectance'] > 0]    # Drop where the reflectance is 0 (probably an error?)
                    data = data[data['Wavelength(micron)'] >= 0.2 ]  # Drop wher wavlength = 0 or i.e. 0.001 (probably error?)

                    data.sort_values(by=['Wavelength(micron)'], inplace=True)   # make sure they are sorted propery

                    # drop duplicates -- because of interpolate.iterp1d
                    data.drop_duplicates(subset=['Wavelength(micron)'], keep='first', inplace=True)

                    if not data.empty:
                        relab_samples.append(split_name[0])
                        client.set(split_name[0], 
                                   json.dumps(dict(wavelengths=data['Wavelength(micron)'].to_list(),
                                                   fluxes=data['Reflectance'].to_list(),
                                                   max_wavelength=data['Wavelength(micron)'].max(),
                                                   min_wavelength=data['Wavelength(micron)'].min(),
                                                   file_name=file_name)))
    
    client.set('relab_samples', json.dumps(relab_samples))
    n_sample_ids = len(json.loads(client.get('relab_samples')))
    print(f'{n_sample_ids} samples stored in redis...')


if __name__ == '__main__':
    refresh_relab()
    parse_raw_data()
