import psycopg2
import pandas
from tqdm import tqdm

NEO_RELAB_DB = 'postgres://bkgbyrlkylzzba:5d515de5ad982d92b7c9ffdf025974822617c1b7cab14110c1a7c162e5f9b98b@ec2-52-71-153-228.compute-1.amazonaws.com:5432/d8i9lq934pv0va'

# table = 'results'
# table = 'results_other_met'
table = 'results_slope_other_met'
# table = 'results_cs_removed_other_met'

QUERY = f"""
SELECT DISTINCT id,
                spectrum_data -> 'asteroid' -> 'wavelength' as a_wavelength,
                spectrum_data -> 'meteorite' -> 'wavelength' AS m_wavelength
FROM neo_relab.{table};"""


def run():

    # get the spectra
    connection = psycopg2.connect(NEO_RELAB_DB)
    data = pandas.read_sql_query(QUERY, connection)
    data['a_min'] = data.a_wavelength.apply(lambda x: min(x))
    data['a_max'] = data.a_wavelength.apply(lambda x: max(x))
    data['m_min'] = data.m_wavelength.apply(lambda x: min(x))
    data['m_max'] = data.m_wavelength.apply(lambda x: max(x))

    data['overlap_start'] = data[['a_min', 'm_min']].max(axis=1)
    data['overlap_stop'] = data[['a_max', 'm_max']].min(axis=1)

    n_spectra = len(data)
    with connection:
        with connection.cursor() as cur:
            for spectrum in tqdm(data.itertuples(), total=n_spectra):

                cur.execute(
                    f"""UPDATE neo_relab.{table} SET overlapstart_microns = %s, overlapstop_microns = %s
                       WHERE id = %s""",
                    (spectrum.overlap_start, spectrum.overlap_stop, spectrum.id))

    connection.close()


if __name__ == '__main__':
    run()
