import math
import pandas
import os

from config import ASTEORID_DATA_DIRECTORY, SMASS_EXCEL, ALBEDO_RANGES


def list_asteroid_data(data_directory=ASTEORID_DATA_DIRECTORY):
    """List the asteroid files in the smass data directory"""
    asteroid_files = os.listdir(data_directory)
    return [f'{data_directory}/{af}' for af in asteroid_files if af != '.DS_Store']


def read_asteroid(file_name, cs_mode={}):
    """Read in the asteroid file, expecting columns of at least wavelength and reflectance.
    Drops where reflectance is <= 0"""
    cs_mode=bool(cs_mode)
    try:
        data = pandas.read_csv(file_name, delim_whitespace=True, header=None)
        data.rename(columns={0: 'wavelength', (1, 2)[cs_mode]: 'reflectance', (2, 3)[cs_mode]: 'error'}, inplace=True)
    except pandas.errors.ParserError as e:
        print(f'File [{file_name}] formatted poorly. Parsing the harder way...')
        data = []
        with open(file_name, 'r') as asteroid_file:
            for line in asteroid_file:
                split_line = line.split()
                if split_line:
                    data.append({'wavelength': float(split_line[0]), 
                                 'reflectance': float(split_line[1]),
                                 'error': float(split_line[2])})

        data = pandas.DataFrame(data)

    data = data[data.reflectance > 0]
    data.sort_values(by=['wavelength'], inplace=True)

    asteroid_name = parse_filename(file_name)
    asteroid_type = types_dict.get(asteroid_name)
    asteroid_data = {'spectrum': data,
                     'asteroid_name': asteroid_name,
                     'type': asteroid_type,
                     'asteroid_file_name': file_name.split('/')[-1],
                     'albedo_range': albedo_range.get(asteroid_type.upper(), (0, 1)) if asteroid_type else (0, 1),
                     'snr_1micron': calculate_snr(data, (0.95, 1.0)),
                     'snr_2micron': calculate_snr(data, (2.1, 2.2))}

    return asteroid_data


def smass_types():
    print(f'Parsing [{SMASS_EXCEL}] for asteroid types...')
    data = pandas.read_excel(SMASS_EXCEL)

    numbers = data.dropna(subset=['Number'])
    numbers = dict(zip(numbers.Number.astype(str).to_list(),
                       numbers.Type.to_list()))

    designations = data.dropna(subset=['Designation'])
    designations = dict(zip(designations.Designation.apply(lambda x: x.upper().replace(' ', '')).to_list(),
                            designations.Type.to_list()))

    return {**numbers, **designations}
types_dict = smass_types()  # make this globally available


def read_albedo_ranges(albedo_file=None):
    albedo_data = pandas.read_excel(albedo_file, skiprows=1)
    albedo_ranges = dict(zip(albedo_data['Unnamed: 0'].str.upper(),
                             tuple(zip(albedo_data['Final low'], albedo_data['Final High']))))
    return albedo_ranges
albedo_range = read_albedo_ranges(ALBEDO_RANGES)    # make this globally available


def calculate_snr(spectrum_df, wavlength_range: tuple):
    data = spectrum_df[spectrum_df.wavelength.between(wavlength_range[0], wavlength_range[1])]
    try:
        err_med = data.error.median()
        if err_med:
            snr = data.reflectance.median() / err_med
            return (snr, None)[math.isinf(snr)]
        return None

    except (ZeroDivisionError, ValueError):
        return None


def parse_filename(x):
    x = x.split('/')[-1].split('.')[0]
    if x[0:2].lower() == 'au':
        return x[2:].upper().replace(' ', '')

    else:
        return str(int(x[1:]))


if __name__ == '__main__':
    files = list_asteroid_data()
    for f in files:
        print(read_asteroid(f))
