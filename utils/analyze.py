import numpy
import logging
import pandas
import json
from collections import namedtuple
from scipy import stats, interpolate, signal
from config import RELAB_CATALOGS_DIRECTORY, client


logger = logging.getLogger(__name__)

MeteoriteMatch = namedtuple('MeteoriteMatch', ['sample_id', 'chisq', 'p_value', 'wavelengths', 'asteroid_flux_filtered',
                                               'meteorite_flux', 'sample_catalog_data', 'spectra_catalog_data',
                                               'albedo_reflectance', 'asteroid_flux_normalized', 'wavelengths_original',
                                               'meteorite_wavelength', 'meteorite_flux_normalized', 'removed_slopes'])


# read and format the relevant catalogs
spectra_catalog = pandas.read_excel(f'{RELAB_CATALOGS_DIRECTORY}/Spectra_Catalogue.xls')
sample_catalog = pandas.read_excel(f'{RELAB_CATALOGS_DIRECTORY}/Sample_Catalogue.xls')

# strip white spaces from any text column
for catalog in [spectra_catalog, sample_catalog]:
    for col in catalog.columns:
        if catalog[col].dtype == 'object':
            catalog[col] = catalog[col].str.strip()

# Set the appropriate indexes
spectra_catalog.set_index('SpectrumID', inplace=True, drop=False)
sample_catalog.set_index('SampleID', inplace=True, drop=False)

# There are duplicates in the catalog files... just keep the first one...
spectra_catalog.drop_duplicates('SpectrumID', inplace=True)
sample_catalog.drop_duplicates('SampleID', inplace=True)

# create empty set of appropriate length
no_sample_data = pandas.Series(index=sample_catalog.columns, dtype='object')


def find_nearest(array, value):
    array = numpy.asarray(array)
    idx = (numpy.abs(array - value)).argmin()
    return idx


def _filter(wavelength, reflectance):
    # sort the asteroid by wavelengths
    wavelength, reflectance = zip(*sorted(zip(wavelength, reflectance)))

    # smooth the asteroid https://scipy.github.io/devdocs/generated/scipy.signal.savgol_filter.html
    filtered_reflectance = signal.savgol_filter(reflectance, 21, 3)

    return wavelength, filtered_reflectance


def _calc_fit(x, y, around=0.55):

    # get the closest wavelength to `normalize_wavelength`
    closest_index, closest_value = min(enumerate(x), key=lambda w: abs(w[1]-around))
    if abs(closest_value - around) > 0.5:
        return None

    closest_index = 10 if closest_index < 10 else closest_index

    # do a first order extrapolation if we are at the very beginning of the meteorite spectrum
    fit_order = 2 if closest_index else 1

    # fit a polynomial around the normalize wavelength (2nd degree)
    fit = numpy.poly1d(numpy.polyfit(x[closest_index - 10:closest_index + 10],
                                     y[closest_index - 10:closest_index + 10], fit_order))
    return fit


def _normalize(wavelength, reflectance, normalization_wavelength=None):

    if not normalization_wavelength:
        normalization_wavelength = 0.55 if min(wavelength) <= 0.55 else 1.215

    # calculate the value at `normalize_wavelength`
    normalization_value = _calc_fit(wavelength, reflectance, normalization_wavelength)(normalization_wavelength)
    normalized_reflectance = [r / normalization_value for r in reflectance]

    return normalized_reflectance, normalization_wavelength


def _find_sample(sample_id):
    sample_id = sample_id.upper()

    spectra_data = spectra_catalog.loc[sample_id]
    sample_data = no_sample_data

    # try an exact match  (i.e., LS-CMP-022-2 ==  LS-CMP-022-2) and then loose matches
    loose_sample_id = '-'.join(spectra_data.SampleID.split('-')[:-1])
    possible_matches = [spectra_data.SampleID, loose_sample_id] + [loose_sample_id[:l] for l in [-1, -2, -4]]
    for match in possible_matches:
        try:
            sample_data = sample_catalog.loc[match]
            logger.debug(f'Sample ID [{spectra_data.SampleID}] matched to [{match}].')
            break
        except KeyError:
            continue

    return sample_data, spectra_data


def _remove_slope(wavelength, reflectance):

    # fit a linear slope
    coeffs = numpy.polyfit(wavelength, reflectance, 1)
    f = numpy.poly1d(coeffs)

    # divide the slope from the reflectance
    flattened_reflectance = [r/x for r, x in zip(reflectance, f(wavelength))]

    return flattened_reflectance, {'m': coeffs[0], 'b': coeffs[1]}


def compare_spectra(asteroid_filename, asteroid_spectrum, albedo_range, keep_top_n=100, match_only='', remove_slope=False, cs_vals={}):

    # when in cs-slope removed mode, don't compare spectra past 1.9 microns
    meteorite_ranks = []

    cs_slope_val = ''
    if cs_vals:
        # grab the cs value
        cs_slope_val = cs_vals.get(asteroid_filename.split('/')[-1])

        # cut the asteroid at 1.9 microns
        asteroid_spectrum = asteroid_spectrum[asteroid_spectrum.wavelength <= 1.9]

    a_wavelength = asteroid_spectrum.wavelength
    a_reflectance = asteroid_spectrum.reflectance
    min_a_wavelength = min(a_wavelength)
    max_a_wavelength = max(a_wavelength)

    a_coeffs = ''
    if remove_slope:
        # print('Removing slope for comparison...')
        a_reflectance, a_coeffs = _remove_slope(a_wavelength, a_reflectance)

    # normalize and filter
    a_reflectance_normalized, normalization_wavelength = _normalize(a_wavelength, a_reflectance)
    a_wavelength, a_reflectance = _filter(a_wavelength, a_reflectance_normalized)

    # print('Comparing against meteorites...')
    relab_samples = json.loads(client.get('relab_samples'))
    for sample_id in relab_samples:

        sample_data, spectra_data = _find_sample(sample_id)
        # only use Source specified by `match_only` if provided. If not a match, continue to next meteorite
        if match_only and str(sample_data.Source).lower() != match_only.lower():
            continue

        # retrieve the meteorite data from redis
        meteorite_spectrum = json.loads(client.get(sample_id))
        # the meteorite spectrum wavelengths must span at least the asteroid spectrum wavelengths
        if min(meteorite_spectrum['wavelengths']) > min_a_wavelength or max(meteorite_spectrum['wavelengths']) < max_a_wavelength:
            continue

        try:
            # use the meteorite to calculate the albedo if meteorite has 0.55 wavelength - reflectance @ 0.55 = albedo
            albedo_fit = _calc_fit(meteorite_spectrum['wavelengths'], meteorite_spectrum['fluxes'])
            albedo = albedo_fit(0.55) if albedo_fit else -1  # albedo=-1 is unphysical

            # only consider this meteorite if it has an acceptable albedo given the asteroid type
            # also accept if no albedo was determined (-1)
            if albedo_range[0] < albedo < albedo_range[1] or albedo == -1:
                m_wavelength = meteorite_spectrum['wavelengths']
                m_reflectance = meteorite_spectrum['fluxes']

                m_coeffs = ''
                if remove_slope:
                    m_reflectance, m_coeffs = _remove_slope(m_wavelength, m_reflectance)

                # Normalize the meteorite spectrum
                f = interpolate.interp1d(m_wavelength, m_reflectance, kind='cubic')

                # Interpolate
                m_reflectance_interp = f(list(a_wavelength))
                m_reflectance_interp, _ = _normalize(a_wavelength, m_reflectance_interp, normalization_wavelength)

                # Quantify how different the lines are
                chisq = stats.chisquare(m_reflectance_interp, a_reflectance)

                meteorite_ranks.append(
                    MeteoriteMatch(sample_id=sample_id,
                                   chisq=chisq[0],
                                   p_value=chisq[1],
                                   albedo_reflectance=albedo,
                                   wavelengths_original=asteroid_spectrum.wavelength,
                                   asteroid_flux_normalized=a_reflectance_normalized,
                                   wavelengths=a_wavelength,
                                   asteroid_flux_filtered=a_reflectance,
                                   meteorite_flux=m_reflectance,
                                   meteorite_flux_normalized=_normalize(m_wavelength,
                                                                        m_reflectance,
                                                                        normalization_wavelength)[0],
                                   meteorite_wavelength=m_wavelength,
                                   sample_catalog_data=sample_data,
                                   spectra_catalog_data=spectra_data,
                                   removed_slopes={'m_coeffs': m_coeffs, 'a_coeffs': a_coeffs, 'cs_slope': cs_slope_val}
                                   )
                )

        except Exception:
            import traceback
            print(traceback.format_exc())
            print(f'Something went wrong comparing to sample file [{sample_id}]. Skipping...')

    # sorts by chisq, smaller is better and keep only 'keep_top_n' number of results
    ranked_results = sorted(meteorite_ranks, key=lambda k: abs(k[1]))[:keep_top_n]
    del meteorite_ranks
    return ranked_results
