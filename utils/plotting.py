import matplotlib.pyplot as plt
from matplotlib import rcParams

rcParams['legend.handlelength'] = 0


def plot_ranks(asteroid, meteorite_ranks):

    asteroid_name = asteroid['asteroid_name']
    asteroid_file_name = asteroid['asteroid_file_name']

    # print(f'Plotting for asteroid [{asteroid_name}]...')
    fig, axs = plt.subplots(3, 5, sharex=True, sharey=True, figsize=(16.5, 10), dpi=300)
    legend_list = []
    legend_labels = []
    n = 0
    for i in range(3):
        for j in range(5):

            # The specific meteorite to compare against
            r = meteorite_ranks[n]

            if n == 0:  # 0 should be the best match
                single_plot(r, asteroid_name, asteroid_file_name)

            axs[i, j].plot(r.wavelengths_original, r.asteroid_flux_normalized, color='tab:blue')
            mplt = axs[i, j].plot(r.meteorite_wavelength, r.meteorite_flux_normalized, label='meteorite', color='tab:orange')[0]
            axs[i, j].legend(loc='upper left', title=f'{n}', frameon=False, labels=[], prop={'size': 5})

            axs[i, j].set_xlim([min(r.wavelengths_original) - 0.05, max(r.wavelengths_original) + 0.05])
            axs[i, j].set_ylim([min(r.asteroid_flux_normalized) - 0.05, max(r.asteroid_flux_normalized) + 0.05])

            # the subtype can be quite long
            subtype = str(r.sample_catalog_data.SubType)
            # if len(subtype.split()) > 3:
            #     subtype = ' '.join(subtype.split().insert(3, '\n'))

            legend_list.append(mplt)
            legend_labels.append(f'{n}. {r.sample_id}: Chi-Squared={round(r.chisq, 4)}\n'
                                 f'Albedo={r.albedo_reflectance:.2f}\n'
                                 f'{r.sample_catalog_data.Source}:{r.sample_catalog_data.GeneralType1}:{r.sample_catalog_data.Type1}\n'
                                 f'{subtype}\n')

            # Hide the right and top spines
            axs[i, j].spines['right'].set_visible(False)
            axs[i, j].spines['top'].set_visible(False)
            if n % 10 != 0:
                axs[i, j].spines['left'].set_visible(False)

            # iterate counter for next plot
            n += 1

    fig.text(0.5, 0.005, 'Wavelength ($\mu$m)', ha='center')
    fig.text(0.04, 0.5, 'Relative Reflectance', va='center', rotation='vertical')
    fig.subplots_adjust(wspace=0, hspace=0, left=0.085, right=0.70, bottom=0.05, top=0.99)
    fig.legend(legend_list, legend_labels, loc='center right',
               title=f"{asteroid_file_name} ({asteroid['type']})", ncol=2, prop={'size': 10}, numpoints=None,
               borderpad=5, labelspacing=1, frameon=False)

    # Save the figure of plots
    fig.savefig(f"./results/plots/{asteroid_file_name.replace('.txt', '')}.png", bbox_inches='tight')

    # Clean up for memory recovery
    fig.clf()
    plt.close(fig)


def single_plot(plot_data, asteroid_name, asteroid_file_name):

    fig = plt.figure(figsize=(8, 5), dpi=300)
    fig.subplots_adjust(wspace=0, hspace=0, left=0.085, right=0.76, bottom=0.05, top=0.99)

    ax = plt.subplot(111)

    ax.plot(plot_data.wavelengths_original, plot_data.asteroid_flux_normalized,
            label=f'Asteroid ({asteroid_name})', linestyle='-', color='tab:blue')
    ax.plot(plot_data.meteorite_wavelength, plot_data.meteorite_flux_normalized, label=f'Sample: {plot_data.sample_id}',
            linestyle='-', color='tab:orange')
    ax.plot(plot_data.wavelengths, plot_data.asteroid_flux_filtered, label=f'Asteroid ({asteroid_name}) (smoothed)',
            linestyle='-', color='tab:green')

    ax.set_xlim([min(plot_data.wavelengths) - 0.05, max(plot_data.wavelengths) + 0.05])
    ax.set_ylim([min(plot_data.asteroid_flux_normalized) - 0.05, max(plot_data.asteroid_flux_normalized) + 0.05])

    fig.legend(loc='upper right', frameon=False, prop={'size': 8}, numpoints=2)
    fig.text(0.99, 0.70,
             f'Chi-Squared: {round(plot_data.chisq, 4)}\nSource: {plot_data.sample_catalog_data.Source}\n'
             f'GeneralType: {plot_data.sample_catalog_data.GeneralType1}\nType1: {plot_data.sample_catalog_data.Type1}\n'
             f'SubType: {plot_data.sample_catalog_data.SubType}\n'
             f'Albedo: {plot_data.albedo_reflectance:.4f}',
             ha='right',
             transform=fig.transFigure,
             size=8)

    fig.savefig(f"./results/plots/best/best_{asteroid_file_name.replace('.txt', '')}.png", bbox_inches='tight')
    fig.clf()
    plt.close(fig)

