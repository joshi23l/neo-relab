# NEO-RELAB

The main goal of this code is to compare all our NEO spectra against all [RELAB spectra](http://www.planetary.brown.edu/relab/) and determine the 'best matches' for all. Many papers have done meteorite matching to their spectra. However, a comprehensive comparison on a large dataset seems to be novel.

## Get Code and Run

1. `clone` the repository (you should set up [ssh keys](https://docs.gitlab.com/ee/ssh/)):

    ```bash
     git clone git@gitlab.com:mit_neos/neo-relab.git
    ```

2. Put and spectra data you want to analyze in `neo-relab/smass/data/`
3. Change into the `neo-relab/` directory
4. Install the python requirements:

    ```bash
   pip install -r requirements 
   ```

5. Run the analysis code:

    ```bash
    python run.py [options]
    ```

6. The results expect a postgresql table `results` to write to. The database string is configurable in `config.py`

Note: Redis is used to cache the relab spectra to be made available across all running threads. Redis will need to be installed and running for the script to work. See the [Redis Quickstart](https://redis.io/topics/quickstart)

## Methods

At a very high level:

1. Each asteroid is normalized.
2. Each asteroid spectrum is smoothed
3. Each meteorite's albedo is calculated to see if it falls in the range of acceptable albedos for the asteroid type
4. Each meteorite spectrum is normalized and interpolated to the same number of points and wavelengths as the asteroid
5. A chi-squared value is determined for each *asteroid&rarr;meteorite* pair
6. The smallest chi-squared asteroid-meteorite value is taken to be the 'best' match

### Spectrum Normalization

Each asteroid and meteorite is normalized to 1 at either 0.55 microns or 1.215 depending on the available wavelengths of the asteroid.
This is accomplished by fitting a polynomial to a small windows around the normalization wavelength to determine the would-be reflectance
at the normalization wavelength. 

Many times, there isn't a value at exactly 0.55 or 1.215 microns, so we use the polynomial to determine the 
would-be reflectance at the normalization wavelength. Another option would be to interpolate the asteroid spectrum and make
a value at exactly 0.55/1.215 microns. However, this alters the original spectrum wavelengths, so we prefer the polynomial method.
This relates to the [Interpolation](#interpolation) method of smoothing described below.

### Asteroid Smoothing

#### Savitzky-Golay

We are currently implementing [Savitzky-Golay filtering](https://en.wikipedia.org/wiki/Savitzky–Golay_filter) to smooth
the asteroid spectrum. Specifically we are using the python [`scipy.signal.savgol_filter`](https://scipy.github.io/devdocs/generated/scipy.signal.savgol_filter.html)
function with a `window_length=21` and a `polyorder=3`. There was no rigorous testing to find an appropriate window
length, simply trial and error. With a polyorder equal to 3, we are fitting a cubic polynomial through the window
length. All other optional parameters are left at their default.

#### Spline

A smoothing spline fit was tried, but ultimately we were not satisfied with its ability to smooth without loosing
meaningful spectral features. We were using the [`scipy.interpolate.UnivariateSpline`](https://docs.scipy.org/doc/scipy/reference/generated/scipy.interpolate.UnivariateSpline.html)
class both with and without weights, and with various smoothing factors and degrees. With a large enough array (x-axis),
we could achieve most of the spectral features. However, this required about the number of points in the original spectral
data and resulted in a barely smoother line.

#### Polynomial Smoothing Fit

An attempt was made to use a simple polynomial fit to the data as a smoothed representation of the data. For this we used
the [`numpy.polynomial`](https://numpy.org/doc/stable/reference/generated/numpy.polyfit.html) function. The results was
a significantly smoother line, but again at the cost of minor spectral features. Either end of the spectrum were generally
very poorly fit, a typical fault of polynomial fitting. Additionally, to well fit complex spectra we required at least
an 8th order polynomial representation. Due to those deficiencies, we decided against a polynomial fit method.

### Interpolation

We interpolated each meteorite spectrum prior to comparing against each asteroid spectrum. For each asteroid, each
meteorite spectrum was interpolated to the same wavelengths as the asteroid spectrum. The RELAB meteorite spectrum generally
have significantly more data points and spans more wavelengths than an asteroid spectrum. Therefore, the result of 
interpolating a meteorite spectrum is essentially just a lower resolution meteorite spectrum. Do note, that we are not
interpolating to a fixed number of points. Rather, we interpolate the meteorite spectrum to the current asteroid
wavelengths. For example, if *asteroid_1* has 12 points, *meteorite_1* will have 12 points for the *asteroid_1&rarr;meteorite_1* 
comparison. But, if *asteroid_2* has 25 points, *meteorite_1* will have 25 points for the *asteroid_2&rarr;meteorite_1 comparison*.
This allows us to keep all the of asteroid spectrum information when comparing to an meteorite spectrum.

For interpolation we are using the [`scipy.interpolat.interp1d`](https://docs.scipy.org/doc/scipy/reference/generated/scipy.interpolate.interp1d.html)
function. We are using a cubic interplation so set `kind='cubic'`, and all other optional parameters are default. 

#### Albedo Constraints

We only consider matching meteorites to asteroids if the albedo of the meteorite would fall in an acceptable range of albedos
for the asteroid type. The albedo of the meteorite is determined by fitting a polynomial to a window around 0.55 microns of the
un-normalized meteorite spectrum. The reflectance of the meteorite at the would-be 0.55 micron wavelength is taken as the 
albedo of the meteorite.

#### Chi-Squared Comparison

We use the Chi-Squared metric to compare how well a meteorite matches an asteroid spectrum. We implement the
[`scipy.stats.chisquare`](https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.chisquare.html) function with 
all default values to perform this comparison. The closer chi-squared is to 0, the better the meteorite matches the
asteroid. We therefore take the chi-squared that is closes to 0 as the 'best match' to the asteroid spectrum. Due note
that incredibly poor fits can result in a negative chi-squared.


### Normalization Then Interpolation
For each asteroid:
1. Normalize the asteroid
    - if the minimum wavelength <= 0.55 microns, normalize at 0.55 microns. Otherwise normalize at 1.215 microns
    - a lot of the time there isn't a 0.55 micron wavelength exactly data point. Rather than interpolate here to get 
      0.55 microns, we fit a polynomial to the asteroid. Then use the polynomial to get the wavelength at the would-be 
      0.55 microns, then divide the smoothed spectrum by that value.

2. Smooth the asteroid (see: [Asteroid Smoothing](#asteroid-smoothing))

For each meteorite:
3. Check the albedo of the meteorite (see: [Albedo Constraints](#albedo-constraints))

4. normalize the meteorite using the same normalization wavelength and method as for normalizing the asteroid. If the 
   asteroid was normalized at 0.55 microns, normalize the meteorite at 0.55 microns. Likewise if the asteroid was 
   normalized at 1.215 microns.
    - again, we fit a polynomial to the meteorite to get the reflectance at 0.55/1.215 microns and then dived 
5. take the normalized meteorite spectrum and interpolate it to match the same/original wavelengths as the asteroid

**Note that we _never_ interpolate the asteroid spectrum**

Our overall intent was to change the spectra the least for comparison.
1. Most, but not all, asteroids have a value already at 0.55 microns (see a004587.visnir.txt). So, we could have 
   interpolated to get a value at 0.55 microns always. This would have either degraded the spectrum by reducing the 
   number of data points, or changed the wavelength values of the spectrum, or both. So, instead we chose the polynomial
   fit to get the would-be value at 0.55 microns and then divide out the spectrum by that value. This keeps all the 
   original data points and wavelengths, just now normalized.
2. We eventually do have to interpolate the meteorite spectrum to get a comparison and they typically have many more 
   data points than the asteroid. However, the asteroid spectrum is not interpolated, it still contains all the original
   wavelengths. So, rather than interpolate the asteroid and meteorite to a fixed number of points, we interpolated the 
   meteorite to the wavelengths of the asteroid. If the asteroid has 100 data points, the interpolated meteorite will 
   have 100 data points. If the asteroid has 10,000 data points the meteorite will have 10,000 data points. Effectively, 
   this allows me to compare the meterorite and asteroid with the maximum number of data points.

### Relab Data Files

#### Parsing

Relab mereorite data files come in two formats, .asc and .txt. We use the .txt files as they are slightly easier to parse and have wavelengths in microns (vs angstroms in the .asc files) which is also the unit used in our spectra files.

We implement one of two methods for parsing the .txt relab files.

1. pandas.read_csv
    - This is the simplest approach, making use of the pandas csv parser. Here we specify to only use the 0 and 1 columns
    of the file (`usecols=[0,1]`) as some text files include more than two columns. The 0th and 1st columns
    are the wavelength in microns and reflectance, respectively.

2. line by line parsing
    - in the case where the above fails, we parse the file line by line. This is longer, but allows us to
    deal with poorly formatted files. The most common reason the above parsing fails is due to poorly formmated
    values in the text file columns. Listed are some example of values found in files that cause the above to fail:
        - `$0.42599`
        - `0>69498`
        - `*`
        - ` 0&04657`

#### Data Cuts

There are a few 'cuts' made to the Relab data that are either of convenience or necessity.

1. We require that the meteorite spectrum spans at least the wavelengths 0.9 through 2.45 microns. If that span is not found in the meteorite spectrum, we do not use it for comparison.

2. We drop any rows of meteorite spectra where the reflectance <= 0

3. We drop any rows of meteorite spectra where the wavelength is < 0.2.

    - In these cases, the data row is usually = 0.001 or similiar, indicating that this is a bad data row. However, even it it is a good data point, we don't consider wavelengths that small in the asteroids comparison. So, it is safe to cut this data

### Comparison Variations

There are several variontion of comparisons that can be run.

1. Removing the linear slope from the asteroid and the meteorite before camparison using the `[--remove-slope]` flag

2. Comparing only to a specified meteorite source using the `[--only-source=<source>]` opition. For example:

    ```bash
    python run.py --only-source Other-Met
    ```

3. Removing just the space weather slope from the asteroid before comparison. For this to work, you must provide the space -weather-removed asteroid spectra files. The `run.py` script does not remove the space weathering slope because we only remove the space weathering slope from the asteroid and not the meteorite. However, you must tell the script that you are running with space-weather-removed data using the `[--cs-mode]` flag.

    - space weathering removal only works well for wavelengths <= 1.9 microns. So, `--cs-mode` explicitly cuts the asteorid and meteorite spectra at 1.9 microns. `--cs-mode` would technically not need to be set if the provided asteroid and meteorite spectra were previously cut at 1.9 microns.
